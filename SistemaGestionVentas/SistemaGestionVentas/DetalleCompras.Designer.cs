﻿
namespace SistemaGestionVentas
{
    partial class DetalleCompras
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox_Det_Comp = new System.Windows.Forms.GroupBox();
            this.id = new System.Windows.Forms.TextBox();
            this.JLBCod_CLI = new System.Windows.Forms.Label();
            this.PREC_DET = new System.Windows.Forms.TextBox();
            this.CANT_DET = new System.Windows.Forms.TextBox();
            this.COD_PROD = new System.Windows.Forms.TextBox();
            this.ID_COMP = new System.Windows.Forms.TextBox();
            this.DET_COMP = new System.Windows.Forms.TextBox();
            this.BTN_AG7 = new System.Windows.Forms.Button();
            this.BTN_ELM7 = new System.Windows.Forms.Button();
            this.BTN_AC7 = new System.Windows.Forms.Button();
            this.JLB_codprd_DET = new System.Windows.Forms.Label();
            this.JLB_PRECIO_DET = new System.Windows.Forms.Label();
            this.JLB_IdComp_DET = new System.Windows.Forms.Label();
            this.JLB_DET_CANT = new System.Windows.Forms.Label();
            this.JLB_Det_Comp = new System.Windows.Forms.Label();
            this.panelDetCOM = new System.Windows.Forms.Panel();
            this.groupBox_Det_Comp.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox_Det_Comp
            // 
            this.groupBox_Det_Comp.BackColor = System.Drawing.Color.LightSteelBlue;
            this.groupBox_Det_Comp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groupBox_Det_Comp.Controls.Add(this.id);
            this.groupBox_Det_Comp.Controls.Add(this.JLBCod_CLI);
            this.groupBox_Det_Comp.Controls.Add(this.PREC_DET);
            this.groupBox_Det_Comp.Controls.Add(this.CANT_DET);
            this.groupBox_Det_Comp.Controls.Add(this.COD_PROD);
            this.groupBox_Det_Comp.Controls.Add(this.ID_COMP);
            this.groupBox_Det_Comp.Controls.Add(this.DET_COMP);
            this.groupBox_Det_Comp.Controls.Add(this.BTN_AG7);
            this.groupBox_Det_Comp.Controls.Add(this.BTN_ELM7);
            this.groupBox_Det_Comp.Controls.Add(this.BTN_AC7);
            this.groupBox_Det_Comp.Controls.Add(this.JLB_codprd_DET);
            this.groupBox_Det_Comp.Controls.Add(this.JLB_PRECIO_DET);
            this.groupBox_Det_Comp.Controls.Add(this.JLB_IdComp_DET);
            this.groupBox_Det_Comp.Controls.Add(this.JLB_DET_CANT);
            this.groupBox_Det_Comp.Controls.Add(this.JLB_Det_Comp);
            this.groupBox_Det_Comp.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox_Det_Comp.Location = new System.Drawing.Point(6, 12);
            this.groupBox_Det_Comp.Name = "groupBox_Det_Comp";
            this.groupBox_Det_Comp.Size = new System.Drawing.Size(1062, 223);
            this.groupBox_Det_Comp.TabIndex = 1;
            this.groupBox_Det_Comp.TabStop = false;
            this.groupBox_Det_Comp.Text = "Datos De Los Detalles De Las Compras";
            // 
            // id
            // 
            this.id.Location = new System.Drawing.Point(892, 110);
            this.id.Name = "id";
            this.id.Size = new System.Drawing.Size(79, 26);
            this.id.TabIndex = 14;
            // 
            // JLBCod_CLI
            // 
            this.JLBCod_CLI.AutoSize = true;
            this.JLBCod_CLI.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLBCod_CLI.Location = new System.Drawing.Point(809, 112);
            this.JLBCod_CLI.Name = "JLBCod_CLI";
            this.JLBCod_CLI.Size = new System.Drawing.Size(82, 19);
            this.JLBCod_CLI.TabIndex = 13;
            this.JLBCod_CLI.Text = "Sub Total:";
            // 
            // PREC_DET
            // 
            this.PREC_DET.Location = new System.Drawing.Point(757, 47);
            this.PREC_DET.Name = "PREC_DET";
            this.PREC_DET.Size = new System.Drawing.Size(214, 26);
            this.PREC_DET.TabIndex = 12;
            // 
            // CANT_DET
            // 
            this.CANT_DET.Location = new System.Drawing.Point(679, 110);
            this.CANT_DET.Name = "CANT_DET";
            this.CANT_DET.Size = new System.Drawing.Size(113, 26);
            this.CANT_DET.TabIndex = 11;
            // 
            // COD_PROD
            // 
            this.COD_PROD.Location = new System.Drawing.Point(249, 166);
            this.COD_PROD.Name = "COD_PROD";
            this.COD_PROD.Size = new System.Drawing.Size(147, 26);
            this.COD_PROD.TabIndex = 10;
            // 
            // ID_COMP
            // 
            this.ID_COMP.Location = new System.Drawing.Point(249, 108);
            this.ID_COMP.Name = "ID_COMP";
            this.ID_COMP.Size = new System.Drawing.Size(249, 26);
            this.ID_COMP.TabIndex = 9;
            // 
            // DET_COMP
            // 
            this.DET_COMP.Location = new System.Drawing.Point(249, 47);
            this.DET_COMP.Name = "DET_COMP";
            this.DET_COMP.Size = new System.Drawing.Size(249, 26);
            this.DET_COMP.TabIndex = 8;
            // 
            // BTN_AG7
            // 
            this.BTN_AG7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.BTN_AG7.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.BTN_AG7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.BTN_AG7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.BTN_AG7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_AG7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_AG7.ForeColor = System.Drawing.Color.White;
            this.BTN_AG7.Location = new System.Drawing.Point(896, 191);
            this.BTN_AG7.Name = "BTN_AG7";
            this.BTN_AG7.Size = new System.Drawing.Size(75, 23);
            this.BTN_AG7.TabIndex = 7;
            this.BTN_AG7.Text = "Agregar";
            this.BTN_AG7.UseVisualStyleBackColor = false;
            // 
            // BTN_ELM7
            // 
            this.BTN_ELM7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BTN_ELM7.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.BTN_ELM7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BTN_ELM7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BTN_ELM7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_ELM7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_ELM7.ForeColor = System.Drawing.Color.White;
            this.BTN_ELM7.Location = new System.Drawing.Point(797, 191);
            this.BTN_ELM7.Name = "BTN_ELM7";
            this.BTN_ELM7.Size = new System.Drawing.Size(75, 23);
            this.BTN_ELM7.TabIndex = 6;
            this.BTN_ELM7.Text = "Eliminar";
            this.BTN_ELM7.UseVisualStyleBackColor = false;
            // 
            // BTN_AC7
            // 
            this.BTN_AC7.BackColor = System.Drawing.Color.DarkBlue;
            this.BTN_AC7.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.BTN_AC7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkBlue;
            this.BTN_AC7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue;
            this.BTN_AC7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_AC7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_AC7.ForeColor = System.Drawing.Color.White;
            this.BTN_AC7.Location = new System.Drawing.Point(691, 191);
            this.BTN_AC7.Name = "BTN_AC7";
            this.BTN_AC7.Size = new System.Drawing.Size(75, 23);
            this.BTN_AC7.TabIndex = 5;
            this.BTN_AC7.Text = "Actualizar";
            this.BTN_AC7.UseVisualStyleBackColor = false;
            // 
            // JLB_codprd_DET
            // 
            this.JLB_codprd_DET.AutoSize = true;
            this.JLB_codprd_DET.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_codprd_DET.Location = new System.Drawing.Point(70, 168);
            this.JLB_codprd_DET.Name = "JLB_codprd_DET";
            this.JLB_codprd_DET.Size = new System.Drawing.Size(173, 19);
            this.JLB_codprd_DET.TabIndex = 4;
            this.JLB_codprd_DET.Text = "Código Del Producto:";
            // 
            // JLB_PRECIO_DET
            // 
            this.JLB_PRECIO_DET.AutoSize = true;
            this.JLB_PRECIO_DET.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_PRECIO_DET.Location = new System.Drawing.Point(586, 49);
            this.JLB_PRECIO_DET.Name = "JLB_PRECIO_DET";
            this.JLB_PRECIO_DET.Size = new System.Drawing.Size(165, 19);
            this.JLB_PRECIO_DET.TabIndex = 3;
            this.JLB_PRECIO_DET.Text = "Precio del producto:";
            // 
            // JLB_IdComp_DET
            // 
            this.JLB_IdComp_DET.AutoSize = true;
            this.JLB_IdComp_DET.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_IdComp_DET.Location = new System.Drawing.Point(146, 110);
            this.JLB_IdComp_DET.Name = "JLB_IdComp_DET";
            this.JLB_IdComp_DET.Size = new System.Drawing.Size(97, 19);
            this.JLB_IdComp_DET.TabIndex = 2;
            this.JLB_IdComp_DET.Text = "ID Compra:";
            // 
            // JLB_DET_CANT
            // 
            this.JLB_DET_CANT.AutoSize = true;
            this.JLB_DET_CANT.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_DET_CANT.Location = new System.Drawing.Point(586, 110);
            this.JLB_DET_CANT.Name = "JLB_DET_CANT";
            this.JLB_DET_CANT.Size = new System.Drawing.Size(87, 19);
            this.JLB_DET_CANT.TabIndex = 1;
            this.JLB_DET_CANT.Text = "Cantidad:";
            // 
            // JLB_Det_Comp
            // 
            this.JLB_Det_Comp.AutoSize = true;
            this.JLB_Det_Comp.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_Det_Comp.Location = new System.Drawing.Point(88, 49);
            this.JLB_Det_Comp.Name = "JLB_Det_Comp";
            this.JLB_Det_Comp.Size = new System.Drawing.Size(155, 19);
            this.JLB_Det_Comp.TabIndex = 0;
            this.JLB_Det_Comp.Text = "ID Detalle Compra:";
            // 
            // panelDetCOM
            // 
            this.panelDetCOM.BackColor = System.Drawing.Color.LightGray;
            this.panelDetCOM.Location = new System.Drawing.Point(6, 241);
            this.panelDetCOM.Name = "panelDetCOM";
            this.panelDetCOM.Size = new System.Drawing.Size(1062, 362);
            this.panelDetCOM.TabIndex = 2;
            // 
            // DetalleCompras
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1080, 615);
            this.Controls.Add(this.panelDetCOM);
            this.Controls.Add(this.groupBox_Det_Comp);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "DetalleCompras";
            this.Text = "DetalleCompras";
            this.groupBox_Det_Comp.ResumeLayout(false);
            this.groupBox_Det_Comp.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox_Det_Comp;
        private System.Windows.Forms.TextBox id;
        private System.Windows.Forms.Label JLBCod_CLI;
        private System.Windows.Forms.TextBox PREC_DET;
        private System.Windows.Forms.TextBox CANT_DET;
        private System.Windows.Forms.TextBox COD_PROD;
        private System.Windows.Forms.TextBox ID_COMP;
        private System.Windows.Forms.TextBox DET_COMP;
        private System.Windows.Forms.Button BTN_AG7;
        private System.Windows.Forms.Button BTN_ELM7;
        private System.Windows.Forms.Button BTN_AC7;
        private System.Windows.Forms.Label JLB_codprd_DET;
        private System.Windows.Forms.Label JLB_PRECIO_DET;
        private System.Windows.Forms.Label JLB_IdComp_DET;
        private System.Windows.Forms.Label JLB_DET_CANT;
        private System.Windows.Forms.Label JLB_Det_Comp;
        private System.Windows.Forms.Panel panelDetCOM;
    }
}