﻿
namespace SistemaGestionVentas
{
    partial class Productos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.JLB_Codigo = new System.Windows.Forms.Label();
            this.JLB_Nombre = new System.Windows.Forms.Label();
            this.JLB_DescP = new System.Windows.Forms.Label();
            this.JLB_Precio = new System.Windows.Forms.Label();
            this.JLB_Exist = new System.Windows.Forms.Label();
            this.groupBox_Productos = new System.Windows.Forms.GroupBox();
            this.Tex_exist = new System.Windows.Forms.TextBox();
            this.Tex_Precio = new System.Windows.Forms.TextBox();
            this.Tex_DescP = new System.Windows.Forms.TextBox();
            this.Tex_NombreP = new System.Windows.Forms.TextBox();
            this.Tex_Cod_Prod = new System.Windows.Forms.TextBox();
            this.BTN_AGR = new System.Windows.Forms.Button();
            this.BTN_ELM = new System.Windows.Forms.Button();
            this.BTN_Act = new System.Windows.Forms.Button();
            this.Panel_Productos = new System.Windows.Forms.Panel();
            this.groupBox_Productos.SuspendLayout();
            this.SuspendLayout();
            // 
            // JLB_Codigo
            // 
            this.JLB_Codigo.AutoSize = true;
            this.JLB_Codigo.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_Codigo.Location = new System.Drawing.Point(147, 53);
            this.JLB_Codigo.Name = "JLB_Codigo";
            this.JLB_Codigo.Size = new System.Drawing.Size(71, 19);
            this.JLB_Codigo.TabIndex = 0;
            this.JLB_Codigo.Text = "Código:";
            // 
            // JLB_Nombre
            // 
            this.JLB_Nombre.AutoSize = true;
            this.JLB_Nombre.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_Nombre.Location = new System.Drawing.Point(68, 112);
            this.JLB_Nombre.Name = "JLB_Nombre";
            this.JLB_Nombre.Size = new System.Drawing.Size(150, 19);
            this.JLB_Nombre.TabIndex = 1;
            this.JLB_Nombre.Text = "Nombre Producto:";
            // 
            // JLB_DescP
            // 
            this.JLB_DescP.AutoSize = true;
            this.JLB_DescP.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_DescP.Location = new System.Drawing.Point(114, 180);
            this.JLB_DescP.Name = "JLB_DescP";
            this.JLB_DescP.Size = new System.Drawing.Size(104, 19);
            this.JLB_DescP.TabIndex = 2;
            this.JLB_DescP.Text = "Descripcion:";
            // 
            // JLB_Precio
            // 
            this.JLB_Precio.AutoSize = true;
            this.JLB_Precio.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_Precio.Location = new System.Drawing.Point(655, 112);
            this.JLB_Precio.Name = "JLB_Precio";
            this.JLB_Precio.Size = new System.Drawing.Size(61, 19);
            this.JLB_Precio.TabIndex = 3;
            this.JLB_Precio.Text = "Precio:";
            // 
            // JLB_Exist
            // 
            this.JLB_Exist.AutoSize = true;
            this.JLB_Exist.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_Exist.Location = new System.Drawing.Point(627, 53);
            this.JLB_Exist.Name = "JLB_Exist";
            this.JLB_Exist.Size = new System.Drawing.Size(89, 19);
            this.JLB_Exist.TabIndex = 4;
            this.JLB_Exist.Text = "Existencia:";
            // 
            // groupBox_Productos
            // 
            this.groupBox_Productos.BackColor = System.Drawing.Color.LightSteelBlue;
            this.groupBox_Productos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.groupBox_Productos.Controls.Add(this.Tex_exist);
            this.groupBox_Productos.Controls.Add(this.Tex_Precio);
            this.groupBox_Productos.Controls.Add(this.Tex_DescP);
            this.groupBox_Productos.Controls.Add(this.Tex_NombreP);
            this.groupBox_Productos.Controls.Add(this.Tex_Cod_Prod);
            this.groupBox_Productos.Controls.Add(this.BTN_AGR);
            this.groupBox_Productos.Controls.Add(this.BTN_ELM);
            this.groupBox_Productos.Controls.Add(this.BTN_Act);
            this.groupBox_Productos.Controls.Add(this.JLB_Nombre);
            this.groupBox_Productos.Controls.Add(this.JLB_Codigo);
            this.groupBox_Productos.Controls.Add(this.JLB_Exist);
            this.groupBox_Productos.Controls.Add(this.JLB_DescP);
            this.groupBox_Productos.Controls.Add(this.JLB_Precio);
            this.groupBox_Productos.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox_Productos.Location = new System.Drawing.Point(6, 12);
            this.groupBox_Productos.Name = "groupBox_Productos";
            this.groupBox_Productos.Size = new System.Drawing.Size(1062, 223);
            this.groupBox_Productos.TabIndex = 5;
            this.groupBox_Productos.TabStop = false;
            this.groupBox_Productos.Text = "Datos De Los Productos";
            // 
            // Tex_exist
            // 
            this.Tex_exist.Location = new System.Drawing.Point(722, 49);
            this.Tex_exist.Name = "Tex_exist";
            this.Tex_exist.Size = new System.Drawing.Size(211, 26);
            this.Tex_exist.TabIndex = 12;
            // 
            // Tex_Precio
            // 
            this.Tex_Precio.Location = new System.Drawing.Point(722, 108);
            this.Tex_Precio.Name = "Tex_Precio";
            this.Tex_Precio.Size = new System.Drawing.Size(211, 26);
            this.Tex_Precio.TabIndex = 11;
            // 
            // Tex_DescP
            // 
            this.Tex_DescP.Location = new System.Drawing.Point(224, 161);
            this.Tex_DescP.Multiline = true;
            this.Tex_DescP.Name = "Tex_DescP";
            this.Tex_DescP.Size = new System.Drawing.Size(348, 56);
            this.Tex_DescP.TabIndex = 10;
            // 
            // Tex_NombreP
            // 
            this.Tex_NombreP.Location = new System.Drawing.Point(224, 108);
            this.Tex_NombreP.Name = "Tex_NombreP";
            this.Tex_NombreP.Size = new System.Drawing.Size(348, 26);
            this.Tex_NombreP.TabIndex = 9;
            // 
            // Tex_Cod_Prod
            // 
            this.Tex_Cod_Prod.Location = new System.Drawing.Point(224, 49);
            this.Tex_Cod_Prod.Name = "Tex_Cod_Prod";
            this.Tex_Cod_Prod.Size = new System.Drawing.Size(158, 26);
            this.Tex_Cod_Prod.TabIndex = 8;
            // 
            // BTN_AGR
            // 
            this.BTN_AGR.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.BTN_AGR.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.BTN_AGR.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.BTN_AGR.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.BTN_AGR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_AGR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_AGR.ForeColor = System.Drawing.Color.White;
            this.BTN_AGR.Location = new System.Drawing.Point(896, 191);
            this.BTN_AGR.Name = "BTN_AGR";
            this.BTN_AGR.Size = new System.Drawing.Size(75, 23);
            this.BTN_AGR.TabIndex = 7;
            this.BTN_AGR.Text = "Agregar";
            this.BTN_AGR.UseVisualStyleBackColor = false;
            // 
            // BTN_ELM
            // 
            this.BTN_ELM.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BTN_ELM.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.BTN_ELM.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BTN_ELM.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BTN_ELM.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_ELM.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_ELM.ForeColor = System.Drawing.Color.White;
            this.BTN_ELM.Location = new System.Drawing.Point(797, 191);
            this.BTN_ELM.Name = "BTN_ELM";
            this.BTN_ELM.Size = new System.Drawing.Size(75, 23);
            this.BTN_ELM.TabIndex = 6;
            this.BTN_ELM.Text = "Eliminar";
            this.BTN_ELM.UseVisualStyleBackColor = false;
            // 
            // BTN_Act
            // 
            this.BTN_Act.BackColor = System.Drawing.Color.DarkBlue;
            this.BTN_Act.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.BTN_Act.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkBlue;
            this.BTN_Act.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue;
            this.BTN_Act.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_Act.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_Act.ForeColor = System.Drawing.Color.White;
            this.BTN_Act.Location = new System.Drawing.Point(691, 191);
            this.BTN_Act.Name = "BTN_Act";
            this.BTN_Act.Size = new System.Drawing.Size(75, 23);
            this.BTN_Act.TabIndex = 6;
            this.BTN_Act.Text = "Actualizar";
            this.BTN_Act.UseVisualStyleBackColor = false;
            // 
            // Panel_Productos
            // 
            this.Panel_Productos.BackColor = System.Drawing.Color.LightGray;
            this.Panel_Productos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Panel_Productos.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.Panel_Productos.Location = new System.Drawing.Point(6, 241);
            this.Panel_Productos.Name = "Panel_Productos";
            this.Panel_Productos.Size = new System.Drawing.Size(1062, 362);
            this.Panel_Productos.TabIndex = 6;
            // 
            // Productos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1080, 615);
            this.Controls.Add(this.Panel_Productos);
            this.Controls.Add(this.groupBox_Productos);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Productos";
            this.Text = "Productos";
            this.groupBox_Productos.ResumeLayout(false);
            this.groupBox_Productos.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label JLB_Codigo;
        private System.Windows.Forms.Label JLB_Nombre;
        private System.Windows.Forms.Label JLB_DescP;
        private System.Windows.Forms.Label JLB_Precio;
        private System.Windows.Forms.Label JLB_Exist;
        private System.Windows.Forms.GroupBox groupBox_Productos;
        private System.Windows.Forms.TextBox Tex_exist;
        private System.Windows.Forms.TextBox Tex_Precio;
        private System.Windows.Forms.TextBox Tex_DescP;
        private System.Windows.Forms.TextBox Tex_NombreP;
        private System.Windows.Forms.TextBox Tex_Cod_Prod;
        private System.Windows.Forms.Button BTN_AGR;
        private System.Windows.Forms.Button BTN_ELM;
        private System.Windows.Forms.Button BTN_Act;
        private System.Windows.Forms.Panel Panel_Productos;
    }
}