﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Dominio;


namespace SistemaGestionVentas
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg,
            int wparam, int lparam);






        private void Login_Load(object sender, EventArgs e)
        {

        }

        

        private void usuario_Enter(object sender, EventArgs e)
        {
            if(usuario.Text == "usuario")
            {
                usuario.Text = "";
                usuario.ForeColor = Color.LightGray;
            }
        }

        private void usuario_Leave(object sender, EventArgs e)
        {
            if (usuario.Text == "")
            {
                usuario.Text = "Usuario";
                usuario.ForeColor = Color.DarkGray;
            }
        }

        private void contraseña_Enter(object sender, EventArgs e)
        {
            if (contraseña.Text == "Contraseña")
            {
                contraseña.Text = "";
                contraseña.ForeColor = Color.LightGray;
                contraseña.UseSystemPasswordChar = true;
            }
        }

        private void contraseña_Leave(object sender, EventArgs e)
        {
            if (contraseña.Text == "")
            {
                contraseña.Text = "Contraseña";
                contraseña.ForeColor = Color.DarkGray;
                contraseña.UseSystemPasswordChar = false;
            }
        }

        private void BTN_Cerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void BTN_Minimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void Login_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void BTN_Acceder_Click(object sender, EventArgs e)
        {
            if (usuario.Text != "Usuario") {

                if (contraseña.Text != "Contraseña") {

                    UserModel user = new UserModel();
                    var validlogin = user.LoginUser(usuario.Text, contraseña.Text);
                    if (validlogin == true)
                    {
                        InicioForm Inicio = new InicioForm();
                        Inicio.Show();
                        Inicio.FormClosed += logout;
                        this.Hide();
                    }
                    else
                    {
                        msgerror("Usuario o Contraseña Incorrecto. \n    Por Favor Intente Nuevamente");
                        contraseña.Text = "Contraseña";
                        usuario.Focus();


                    }
                }
                else msgerror("Por Favor Ingrese Su Contraseña");

            }
            else msgerror("Por Favor Ingrese Su Nombre De Usuario");

            }

        private void msgerror(string msg)
        {
          JLBerror.Text = "" + msg;
          JLBerror.Visible = true;
        }
        // es temporal solo para mostrar el avance
        //this.Hide();
        //InicioForm n1 = new InicioForm();
        //n1.Show();

        private void logout(object sender, FormClosedEventArgs e)
        {
            contraseña.Text = "Contraseña";
            contraseña.UseSystemPasswordChar = false;
            usuario.Text = "Usuario";
            JLBerror.Visible = false;
            this.Show();
            //contraseña.Clear();
            //usuario.Clear();
            //JLBerror.Visible = false;
            //this.Show();
            //usuario.Focus();

        }


        }
   
    }

