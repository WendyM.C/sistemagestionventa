﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace SistemaGestionVentas
{
    public partial class InicioForm : Form
    {
        public InicioForm()
        {
            InitializeComponent();
        }

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg,
            int wparam, int lparam);




        private void BTNcerrar_inicio_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void BTNminimizar_inicio_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            BTNMaximizar_inicio.Visible = false;
            BTNRestaurar.Visible = true;
        }

        private void BTNRestaurar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            BTNRestaurar.Visible = false;
            BTNMaximizar_inicio.Visible = true;
        }

        private void PanelContenedor_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel8_Paint(object sender, PaintEventArgs e)
        {

        }

        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void panel8_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void pictureBox3_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void Reporte_Click(object sender, EventArgs e)
        {
            Sub_menuRep.Visible = true;
        }

        private void Det_com_Click(object sender, EventArgs e)
        {
            Sub_menuRep.Visible = false;
            AbrirFormHija(new DetalleCompras());
        }

        private void Det_ventas_Click(object sender, EventArgs e)
        {
            Sub_menuRep.Visible = false;
            AbrirFormHija(new DetalleVentas());
        }

        private void Det_Devolucion_Click(object sender, EventArgs e)
        {
            Sub_menuRep.Visible = false;
            AbrirFormHija(new DetalleDevoluciones());
        }

        private void BTN_Cerrarsesion_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Desea Salir De La Aplicación?", "Warning", MessageBoxButtons
                .YesNo, MessageBoxIcon.Warning) == DialogResult.Yes) ;
                this.Close();

            //Application.Exit();
        }

        private void AbrirFormHija( object formhija)
        {
            if (this.PanelContenedor.Controls.Count > 0)
                this.PanelContenedor.Controls.RemoveAt(0);      //primero preguntamos si existe algun control en el interior  del 
            Form fh = formhija as Form;                                                 //    panel, de ser verdadero  lo eliminamos 
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill;
            this.PanelContenedor.Controls.Add(fh);
            this.PanelContenedor.Tag = fh;
            fh.Show();


        }

        private void button2_Click(object sender, EventArgs e)
        {
            AbrirFormHija(new Productos());
        }

        private void Cliente_Click(object sender, EventArgs e)
        {
            AbrirFormHija(new Clientes());
        }

        private void venta_Click(object sender, EventArgs e)
        {
            AbrirFormHija(new Ventas());
        }

        private void Compra_Click(object sender, EventArgs e)
        {
            AbrirFormHija(new Compras());
        }

        private void devoluciones_Click(object sender, EventArgs e)
        {
            AbrirFormHija(new Devoluciones());
        }

        private void Tipo_Producto_Click(object sender, EventArgs e)
        {
            AbrirFormHija(new TipoProductos());
        }
    }


}
