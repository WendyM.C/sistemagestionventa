﻿
namespace SistemaGestionVentas
{
    partial class TipoProductos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelTipoPrd = new System.Windows.Forms.Panel();
            this.JLB_IDtipo = new System.Windows.Forms.Label();
            this.JLB_SNombre = new System.Windows.Forms.Label();
            this.BTN_AC6 = new System.Windows.Forms.Button();
            this.BTN_ELM6 = new System.Windows.Forms.Button();
            this.BTN_AG6 = new System.Windows.Forms.Button();
            this.TipoP = new System.Windows.Forms.TextBox();
            this.NombreTP = new System.Windows.Forms.TextBox();
            this.groupBox_TipoPrd = new System.Windows.Forms.GroupBox();
            this.groupBox_TipoPrd.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelTipoPrd
            // 
            this.panelTipoPrd.BackColor = System.Drawing.Color.LightGray;
            this.panelTipoPrd.Location = new System.Drawing.Point(6, 241);
            this.panelTipoPrd.Name = "panelTipoPrd";
            this.panelTipoPrd.Size = new System.Drawing.Size(1062, 362);
            this.panelTipoPrd.TabIndex = 2;
            // 
            // JLB_IDtipo
            // 
            this.JLB_IDtipo.AutoSize = true;
            this.JLB_IDtipo.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_IDtipo.Location = new System.Drawing.Point(80, 95);
            this.JLB_IDtipo.Name = "JLB_IDtipo";
            this.JLB_IDtipo.Size = new System.Drawing.Size(162, 19);
            this.JLB_IDtipo.TabIndex = 0;
            this.JLB_IDtipo.Text = "ID Tipo De Producto:";
            // 
            // JLB_SNombre
            // 
            this.JLB_SNombre.AutoSize = true;
            this.JLB_SNombre.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_SNombre.Location = new System.Drawing.Point(559, 95);
            this.JLB_SNombre.Name = "JLB_SNombre";
            this.JLB_SNombre.Size = new System.Drawing.Size(142, 19);
            this.JLB_SNombre.TabIndex = 1;
            this.JLB_SNombre.Text = "Nombre Del Tipo:";
            // 
            // BTN_AC6
            // 
            this.BTN_AC6.BackColor = System.Drawing.Color.DarkBlue;
            this.BTN_AC6.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.BTN_AC6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkBlue;
            this.BTN_AC6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue;
            this.BTN_AC6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_AC6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_AC6.ForeColor = System.Drawing.Color.White;
            this.BTN_AC6.Location = new System.Drawing.Point(691, 191);
            this.BTN_AC6.Name = "BTN_AC6";
            this.BTN_AC6.Size = new System.Drawing.Size(75, 23);
            this.BTN_AC6.TabIndex = 5;
            this.BTN_AC6.Text = "Actualizar";
            this.BTN_AC6.UseVisualStyleBackColor = false;
            // 
            // BTN_ELM6
            // 
            this.BTN_ELM6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BTN_ELM6.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.BTN_ELM6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BTN_ELM6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BTN_ELM6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_ELM6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_ELM6.ForeColor = System.Drawing.Color.White;
            this.BTN_ELM6.Location = new System.Drawing.Point(797, 191);
            this.BTN_ELM6.Name = "BTN_ELM6";
            this.BTN_ELM6.Size = new System.Drawing.Size(75, 23);
            this.BTN_ELM6.TabIndex = 6;
            this.BTN_ELM6.Text = "Eliminar";
            this.BTN_ELM6.UseVisualStyleBackColor = false;
            // 
            // BTN_AG6
            // 
            this.BTN_AG6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.BTN_AG6.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.BTN_AG6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.BTN_AG6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.BTN_AG6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_AG6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_AG6.ForeColor = System.Drawing.Color.White;
            this.BTN_AG6.Location = new System.Drawing.Point(896, 191);
            this.BTN_AG6.Name = "BTN_AG6";
            this.BTN_AG6.Size = new System.Drawing.Size(75, 23);
            this.BTN_AG6.TabIndex = 7;
            this.BTN_AG6.Text = "Agregar";
            this.BTN_AG6.UseVisualStyleBackColor = false;
            // 
            // TipoP
            // 
            this.TipoP.Location = new System.Drawing.Point(248, 95);
            this.TipoP.Name = "TipoP";
            this.TipoP.Size = new System.Drawing.Size(249, 26);
            this.TipoP.TabIndex = 8;
            // 
            // NombreTP
            // 
            this.NombreTP.Location = new System.Drawing.Point(707, 93);
            this.NombreTP.Name = "NombreTP";
            this.NombreTP.Size = new System.Drawing.Size(249, 26);
            this.NombreTP.TabIndex = 11;
            // 
            // groupBox_TipoPrd
            // 
            this.groupBox_TipoPrd.BackColor = System.Drawing.Color.LightSteelBlue;
            this.groupBox_TipoPrd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groupBox_TipoPrd.Controls.Add(this.NombreTP);
            this.groupBox_TipoPrd.Controls.Add(this.TipoP);
            this.groupBox_TipoPrd.Controls.Add(this.BTN_AG6);
            this.groupBox_TipoPrd.Controls.Add(this.BTN_ELM6);
            this.groupBox_TipoPrd.Controls.Add(this.BTN_AC6);
            this.groupBox_TipoPrd.Controls.Add(this.JLB_SNombre);
            this.groupBox_TipoPrd.Controls.Add(this.JLB_IDtipo);
            this.groupBox_TipoPrd.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox_TipoPrd.Location = new System.Drawing.Point(6, 12);
            this.groupBox_TipoPrd.Name = "groupBox_TipoPrd";
            this.groupBox_TipoPrd.Size = new System.Drawing.Size(1062, 223);
            this.groupBox_TipoPrd.TabIndex = 1;
            this.groupBox_TipoPrd.TabStop = false;
            this.groupBox_TipoPrd.Text = "Datos De Los Tipos De Productos";
            // 
            // TipoProductos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1080, 615);
            this.Controls.Add(this.panelTipoPrd);
            this.Controls.Add(this.groupBox_TipoPrd);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "TipoProductos";
            this.Text = "TipoProductos";
            this.groupBox_TipoPrd.ResumeLayout(false);
            this.groupBox_TipoPrd.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panelTipoPrd;
        private System.Windows.Forms.Label JLB_IDtipo;
        private System.Windows.Forms.Label JLB_SNombre;
        private System.Windows.Forms.Button BTN_AC6;
        private System.Windows.Forms.Button BTN_ELM6;
        private System.Windows.Forms.Button BTN_AG6;
        private System.Windows.Forms.TextBox TipoP;
        private System.Windows.Forms.TextBox NombreTP;
        private System.Windows.Forms.GroupBox groupBox_TipoPrd;
    }
}