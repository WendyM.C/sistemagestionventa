﻿
namespace SistemaGestionVentas
{
    partial class Clientes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox_Clientes = new System.Windows.Forms.GroupBox();
            this.id = new System.Windows.Forms.TextBox();
            this.JLBCod_CLI = new System.Windows.Forms.Label();
            this.SAcliente = new System.Windows.Forms.TextBox();
            this.SNcliente = new System.Windows.Forms.TextBox();
            this.telefono = new System.Windows.Forms.TextBox();
            this.PAcliente = new System.Windows.Forms.TextBox();
            this.PNombreC = new System.Windows.Forms.TextBox();
            this.BTN_AG2 = new System.Windows.Forms.Button();
            this.BTN_ELM2 = new System.Windows.Forms.Button();
            this.BTN_AC2 = new System.Windows.Forms.Button();
            this.JLB_TEL = new System.Windows.Forms.Label();
            this.JLB_SApellido = new System.Windows.Forms.Label();
            this.JLB_PApellido = new System.Windows.Forms.Label();
            this.JLB_SNombre = new System.Windows.Forms.Label();
            this.JLB_PNombre = new System.Windows.Forms.Label();
            this.panelclientes = new System.Windows.Forms.Panel();
            this.groupBox_Clientes.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox_Clientes
            // 
            this.groupBox_Clientes.BackColor = System.Drawing.Color.LightSteelBlue;
            this.groupBox_Clientes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groupBox_Clientes.Controls.Add(this.id);
            this.groupBox_Clientes.Controls.Add(this.JLBCod_CLI);
            this.groupBox_Clientes.Controls.Add(this.SAcliente);
            this.groupBox_Clientes.Controls.Add(this.SNcliente);
            this.groupBox_Clientes.Controls.Add(this.telefono);
            this.groupBox_Clientes.Controls.Add(this.PAcliente);
            this.groupBox_Clientes.Controls.Add(this.PNombreC);
            this.groupBox_Clientes.Controls.Add(this.BTN_AG2);
            this.groupBox_Clientes.Controls.Add(this.BTN_ELM2);
            this.groupBox_Clientes.Controls.Add(this.BTN_AC2);
            this.groupBox_Clientes.Controls.Add(this.JLB_TEL);
            this.groupBox_Clientes.Controls.Add(this.JLB_SApellido);
            this.groupBox_Clientes.Controls.Add(this.JLB_PApellido);
            this.groupBox_Clientes.Controls.Add(this.JLB_SNombre);
            this.groupBox_Clientes.Controls.Add(this.JLB_PNombre);
            this.groupBox_Clientes.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox_Clientes.Location = new System.Drawing.Point(6, 12);
            this.groupBox_Clientes.Name = "groupBox_Clientes";
            this.groupBox_Clientes.Size = new System.Drawing.Size(1062, 223);
            this.groupBox_Clientes.TabIndex = 0;
            this.groupBox_Clientes.TabStop = false;
            this.groupBox_Clientes.Text = "Datos De Los Clientes";
            // 
            // id
            // 
            this.id.Location = new System.Drawing.Point(408, 175);
            this.id.Name = "id";
            this.id.Size = new System.Drawing.Size(65, 26);
            this.id.TabIndex = 14;
            // 
            // JLBCod_CLI
            // 
            this.JLBCod_CLI.AutoSize = true;
            this.JLBCod_CLI.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLBCod_CLI.Location = new System.Drawing.Point(374, 179);
            this.JLBCod_CLI.Name = "JLBCod_CLI";
            this.JLBCod_CLI.Size = new System.Drawing.Size(28, 19);
            this.JLBCod_CLI.TabIndex = 13;
            this.JLBCod_CLI.Text = "ID:";
            // 
            // SAcliente
            // 
            this.SAcliente.Location = new System.Drawing.Point(722, 108);
            this.SAcliente.Name = "SAcliente";
            this.SAcliente.Size = new System.Drawing.Size(249, 26);
            this.SAcliente.TabIndex = 12;
            // 
            // SNcliente
            // 
            this.SNcliente.Location = new System.Drawing.Point(722, 49);
            this.SNcliente.Name = "SNcliente";
            this.SNcliente.Size = new System.Drawing.Size(249, 26);
            this.SNcliente.TabIndex = 11;
            // 
            // telefono
            // 
            this.telefono.Location = new System.Drawing.Point(224, 175);
            this.telefono.Name = "telefono";
            this.telefono.Size = new System.Drawing.Size(144, 26);
            this.telefono.TabIndex = 10;
            // 
            // PAcliente
            // 
            this.PAcliente.Location = new System.Drawing.Point(224, 108);
            this.PAcliente.Name = "PAcliente";
            this.PAcliente.Size = new System.Drawing.Size(249, 26);
            this.PAcliente.TabIndex = 9;
            // 
            // PNombreC
            // 
            this.PNombreC.Location = new System.Drawing.Point(224, 49);
            this.PNombreC.Name = "PNombreC";
            this.PNombreC.Size = new System.Drawing.Size(249, 26);
            this.PNombreC.TabIndex = 8;
            // 
            // BTN_AG2
            // 
            this.BTN_AG2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.BTN_AG2.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.BTN_AG2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.BTN_AG2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.BTN_AG2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_AG2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_AG2.ForeColor = System.Drawing.Color.White;
            this.BTN_AG2.Location = new System.Drawing.Point(896, 191);
            this.BTN_AG2.Name = "BTN_AG2";
            this.BTN_AG2.Size = new System.Drawing.Size(75, 23);
            this.BTN_AG2.TabIndex = 7;
            this.BTN_AG2.Text = "Agregar";
            this.BTN_AG2.UseVisualStyleBackColor = false;
            // 
            // BTN_ELM2
            // 
            this.BTN_ELM2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BTN_ELM2.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.BTN_ELM2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BTN_ELM2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BTN_ELM2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_ELM2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_ELM2.ForeColor = System.Drawing.Color.White;
            this.BTN_ELM2.Location = new System.Drawing.Point(797, 191);
            this.BTN_ELM2.Name = "BTN_ELM2";
            this.BTN_ELM2.Size = new System.Drawing.Size(75, 23);
            this.BTN_ELM2.TabIndex = 6;
            this.BTN_ELM2.Text = "Eliminar";
            this.BTN_ELM2.UseVisualStyleBackColor = false;
            // 
            // BTN_AC2
            // 
            this.BTN_AC2.BackColor = System.Drawing.Color.DarkBlue;
            this.BTN_AC2.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.BTN_AC2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkBlue;
            this.BTN_AC2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue;
            this.BTN_AC2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_AC2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_AC2.ForeColor = System.Drawing.Color.White;
            this.BTN_AC2.Location = new System.Drawing.Point(691, 191);
            this.BTN_AC2.Name = "BTN_AC2";
            this.BTN_AC2.Size = new System.Drawing.Size(75, 23);
            this.BTN_AC2.TabIndex = 5;
            this.BTN_AC2.Text = "Actualizar";
            this.BTN_AC2.UseVisualStyleBackColor = false;
            // 
            // JLB_TEL
            // 
            this.JLB_TEL.AutoSize = true;
            this.JLB_TEL.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_TEL.Location = new System.Drawing.Point(140, 175);
            this.JLB_TEL.Name = "JLB_TEL";
            this.JLB_TEL.Size = new System.Drawing.Size(78, 19);
            this.JLB_TEL.TabIndex = 4;
            this.JLB_TEL.Text = "Teléfono:";
            // 
            // JLB_SApellido
            // 
            this.JLB_SApellido.AutoSize = true;
            this.JLB_SApellido.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_SApellido.Location = new System.Drawing.Point(565, 112);
            this.JLB_SApellido.Name = "JLB_SApellido";
            this.JLB_SApellido.Size = new System.Drawing.Size(153, 19);
            this.JLB_SApellido.TabIndex = 3;
            this.JLB_SApellido.Text = "Segundo Apellido:";
            // 
            // JLB_PApellido
            // 
            this.JLB_PApellido.AutoSize = true;
            this.JLB_PApellido.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_PApellido.Location = new System.Drawing.Point(86, 108);
            this.JLB_PApellido.Name = "JLB_PApellido";
            this.JLB_PApellido.Size = new System.Drawing.Size(132, 19);
            this.JLB_PApellido.TabIndex = 2;
            this.JLB_PApellido.Text = "Primer Apellido:";
            // 
            // JLB_SNombre
            // 
            this.JLB_SNombre.AutoSize = true;
            this.JLB_SNombre.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_SNombre.Location = new System.Drawing.Point(565, 49);
            this.JLB_SNombre.Name = "JLB_SNombre";
            this.JLB_SNombre.Size = new System.Drawing.Size(151, 19);
            this.JLB_SNombre.TabIndex = 1;
            this.JLB_SNombre.Text = "Segundo Nombre:";
            // 
            // JLB_PNombre
            // 
            this.JLB_PNombre.AutoSize = true;
            this.JLB_PNombre.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_PNombre.Location = new System.Drawing.Point(88, 49);
            this.JLB_PNombre.Name = "JLB_PNombre";
            this.JLB_PNombre.Size = new System.Drawing.Size(130, 19);
            this.JLB_PNombre.TabIndex = 0;
            this.JLB_PNombre.Text = "Primer Nombre:";
            // 
            // panelclientes
            // 
            this.panelclientes.BackColor = System.Drawing.Color.LightGray;
            this.panelclientes.Location = new System.Drawing.Point(6, 241);
            this.panelclientes.Name = "panelclientes";
            this.panelclientes.Size = new System.Drawing.Size(1062, 362);
            this.panelclientes.TabIndex = 1;
            // 
            // Clientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1080, 615);
            this.Controls.Add(this.panelclientes);
            this.Controls.Add(this.groupBox_Clientes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Clientes";
            this.Text = "cLIENTES";
            this.groupBox_Clientes.ResumeLayout(false);
            this.groupBox_Clientes.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox_Clientes;
        private System.Windows.Forms.Panel panelclientes;
        private System.Windows.Forms.Button BTN_AG2;
        private System.Windows.Forms.Button BTN_ELM2;
        private System.Windows.Forms.Button BTN_AC2;
        private System.Windows.Forms.Label JLB_TEL;
        private System.Windows.Forms.Label JLB_SApellido;
        private System.Windows.Forms.Label JLB_PApellido;
        private System.Windows.Forms.Label JLB_SNombre;
        private System.Windows.Forms.Label JLB_PNombre;
        private System.Windows.Forms.TextBox SAcliente;
        private System.Windows.Forms.TextBox SNcliente;
        private System.Windows.Forms.TextBox telefono;
        private System.Windows.Forms.TextBox PAcliente;
        private System.Windows.Forms.TextBox PNombreC;
        private System.Windows.Forms.TextBox id;
        private System.Windows.Forms.Label JLBCod_CLI;
    }
}