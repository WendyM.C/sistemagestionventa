﻿
namespace SistemaGestionVentas
{
    partial class Ventas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.BTN_AG3 = new System.Windows.Forms.Button();
            this.BTN_ELM3 = new System.Windows.Forms.Button();
            this.BTN_AC3 = new System.Windows.Forms.Button();
            this.TotalV = new System.Windows.Forms.TextBox();
            this.ID_Cl = new System.Windows.Forms.TextBox();
            this.Fecha_venta = new System.Windows.Forms.TextBox();
            this.NVenta = new System.Windows.Forms.TextBox();
            this.JLB_CL = new System.Windows.Forms.Label();
            this.JLB_Total = new System.Windows.Forms.Label();
            this.JLB_Fecha = new System.Windows.Forms.Label();
            this.JLB_VENTA = new System.Windows.Forms.Label();
            this.panelventas = new System.Windows.Forms.Panel();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.groupBox1.Controls.Add(this.BTN_AG3);
            this.groupBox1.Controls.Add(this.BTN_ELM3);
            this.groupBox1.Controls.Add(this.BTN_AC3);
            this.groupBox1.Controls.Add(this.TotalV);
            this.groupBox1.Controls.Add(this.ID_Cl);
            this.groupBox1.Controls.Add(this.Fecha_venta);
            this.groupBox1.Controls.Add(this.NVenta);
            this.groupBox1.Controls.Add(this.JLB_CL);
            this.groupBox1.Controls.Add(this.JLB_Total);
            this.groupBox1.Controls.Add(this.JLB_Fecha);
            this.groupBox1.Controls.Add(this.JLB_VENTA);
            this.groupBox1.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(6, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1062, 223);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos De Las Ventas";
            // 
            // BTN_AG3
            // 
            this.BTN_AG3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.BTN_AG3.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.BTN_AG3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.BTN_AG3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.BTN_AG3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_AG3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_AG3.ForeColor = System.Drawing.Color.White;
            this.BTN_AG3.Location = new System.Drawing.Point(896, 191);
            this.BTN_AG3.Name = "BTN_AG3";
            this.BTN_AG3.Size = new System.Drawing.Size(75, 23);
            this.BTN_AG3.TabIndex = 10;
            this.BTN_AG3.Text = "Agregar";
            this.BTN_AG3.UseVisualStyleBackColor = false;
            // 
            // BTN_ELM3
            // 
            this.BTN_ELM3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BTN_ELM3.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.BTN_ELM3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BTN_ELM3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BTN_ELM3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_ELM3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_ELM3.ForeColor = System.Drawing.Color.White;
            this.BTN_ELM3.Location = new System.Drawing.Point(797, 191);
            this.BTN_ELM3.Name = "BTN_ELM3";
            this.BTN_ELM3.Size = new System.Drawing.Size(75, 23);
            this.BTN_ELM3.TabIndex = 9;
            this.BTN_ELM3.Text = "Eliminar";
            this.BTN_ELM3.UseVisualStyleBackColor = false;
            // 
            // BTN_AC3
            // 
            this.BTN_AC3.BackColor = System.Drawing.Color.DarkBlue;
            this.BTN_AC3.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.BTN_AC3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkBlue;
            this.BTN_AC3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue;
            this.BTN_AC3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_AC3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_AC3.ForeColor = System.Drawing.Color.White;
            this.BTN_AC3.Location = new System.Drawing.Point(691, 191);
            this.BTN_AC3.Name = "BTN_AC3";
            this.BTN_AC3.Size = new System.Drawing.Size(75, 23);
            this.BTN_AC3.TabIndex = 8;
            this.BTN_AC3.Text = "Actualizar";
            this.BTN_AC3.UseVisualStyleBackColor = false;
            // 
            // TotalV
            // 
            this.TotalV.Location = new System.Drawing.Point(713, 108);
            this.TotalV.Name = "TotalV";
            this.TotalV.Size = new System.Drawing.Size(203, 26);
            this.TotalV.TabIndex = 7;
            // 
            // ID_Cl
            // 
            this.ID_Cl.Location = new System.Drawing.Point(713, 46);
            this.ID_Cl.Name = "ID_Cl";
            this.ID_Cl.Size = new System.Drawing.Size(203, 26);
            this.ID_Cl.TabIndex = 6;
            // 
            // Fecha_venta
            // 
            this.Fecha_venta.Location = new System.Drawing.Point(264, 115);
            this.Fecha_venta.Name = "Fecha_venta";
            this.Fecha_venta.Size = new System.Drawing.Size(205, 26);
            this.Fecha_venta.TabIndex = 5;
            // 
            // NVenta
            // 
            this.NVenta.Location = new System.Drawing.Point(264, 49);
            this.NVenta.Name = "NVenta";
            this.NVenta.Size = new System.Drawing.Size(205, 26);
            this.NVenta.TabIndex = 4;
            // 
            // JLB_CL
            // 
            this.JLB_CL.AutoSize = true;
            this.JLB_CL.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_CL.Location = new System.Drawing.Point(592, 49);
            this.JLB_CL.Name = "JLB_CL";
            this.JLB_CL.Size = new System.Drawing.Size(115, 19);
            this.JLB_CL.TabIndex = 3;
            this.JLB_CL.Text = "ID del Cliente:";
            // 
            // JLB_Total
            // 
            this.JLB_Total.AutoSize = true;
            this.JLB_Total.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_Total.Location = new System.Drawing.Point(564, 111);
            this.JLB_Total.Name = "JLB_Total";
            this.JLB_Total.Size = new System.Drawing.Size(143, 19);
            this.JLB_Total.TabIndex = 2;
            this.JLB_Total.Text = "Total de la Venta:";
            // 
            // JLB_Fecha
            // 
            this.JLB_Fecha.AutoSize = true;
            this.JLB_Fecha.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_Fecha.Location = new System.Drawing.Point(102, 115);
            this.JLB_Fecha.Name = "JLB_Fecha";
            this.JLB_Fecha.Size = new System.Drawing.Size(156, 19);
            this.JLB_Fecha.TabIndex = 1;
            this.JLB_Fecha.Text = "Fecha de la Venta:";
            // 
            // JLB_VENTA
            // 
            this.JLB_VENTA.AutoSize = true;
            this.JLB_VENTA.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_VENTA.Location = new System.Drawing.Point(88, 49);
            this.JLB_VENTA.Name = "JLB_VENTA";
            this.JLB_VENTA.Size = new System.Drawing.Size(170, 19);
            this.JLB_VENTA.TabIndex = 0;
            this.JLB_VENTA.Text = "Numero de la Venta:";
            // 
            // panelventas
            // 
            this.panelventas.BackColor = System.Drawing.Color.LightGray;
            this.panelventas.Location = new System.Drawing.Point(6, 241);
            this.panelventas.Name = "panelventas";
            this.panelventas.Size = new System.Drawing.Size(1062, 362);
            this.panelventas.TabIndex = 1;
            // 
            // Ventas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1080, 615);
            this.Controls.Add(this.panelventas);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Ventas";
            this.Text = "Ventas";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panelventas;
        private System.Windows.Forms.Label JLB_CL;
        private System.Windows.Forms.Label JLB_Total;
        private System.Windows.Forms.Label JLB_Fecha;
        private System.Windows.Forms.Label JLB_VENTA;
        private System.Windows.Forms.TextBox TotalV;
        private System.Windows.Forms.TextBox ID_Cl;
        private System.Windows.Forms.TextBox Fecha_venta;
        private System.Windows.Forms.TextBox NVenta;
        private System.Windows.Forms.Button BTN_AG3;
        private System.Windows.Forms.Button BTN_ELM3;
        private System.Windows.Forms.Button BTN_AC3;
    }
}