﻿
namespace SistemaGestionVentas
{
    partial class DetalleVentas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelDetVenta = new System.Windows.Forms.Panel();
            this.groupBox_Det_Venta = new System.Windows.Forms.GroupBox();
            this.SUBTO_DET_VENT = new System.Windows.Forms.TextBox();
            this.JLBSUB_VENT = new System.Windows.Forms.Label();
            this.CANT_DET_VENTA = new System.Windows.Forms.TextBox();
            this.COD_PROD_DET_VENT = new System.Windows.Forms.TextBox();
            this.NVENTA_DET_VENT = new System.Windows.Forms.TextBox();
            this.ID_DET_VENT = new System.Windows.Forms.TextBox();
            this.BTN_AG8 = new System.Windows.Forms.Button();
            this.BTN_ELM8 = new System.Windows.Forms.Button();
            this.BTN_AC8 = new System.Windows.Forms.Button();
            this.JLB_codprd_DET = new System.Windows.Forms.Label();
            this.JLB_NVENTA = new System.Windows.Forms.Label();
            this.JLB_DET_CANT_VENT = new System.Windows.Forms.Label();
            this.JLB_Det_venta = new System.Windows.Forms.Label();
            this.groupBox_Det_Venta.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelDetVenta
            // 
            this.panelDetVenta.BackColor = System.Drawing.Color.LightGray;
            this.panelDetVenta.Location = new System.Drawing.Point(6, 241);
            this.panelDetVenta.Name = "panelDetVenta";
            this.panelDetVenta.Size = new System.Drawing.Size(1062, 362);
            this.panelDetVenta.TabIndex = 2;
            // 
            // groupBox_Det_Venta
            // 
            this.groupBox_Det_Venta.BackColor = System.Drawing.Color.LightSteelBlue;
            this.groupBox_Det_Venta.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groupBox_Det_Venta.Controls.Add(this.SUBTO_DET_VENT);
            this.groupBox_Det_Venta.Controls.Add(this.JLBSUB_VENT);
            this.groupBox_Det_Venta.Controls.Add(this.CANT_DET_VENTA);
            this.groupBox_Det_Venta.Controls.Add(this.COD_PROD_DET_VENT);
            this.groupBox_Det_Venta.Controls.Add(this.NVENTA_DET_VENT);
            this.groupBox_Det_Venta.Controls.Add(this.ID_DET_VENT);
            this.groupBox_Det_Venta.Controls.Add(this.BTN_AG8);
            this.groupBox_Det_Venta.Controls.Add(this.BTN_ELM8);
            this.groupBox_Det_Venta.Controls.Add(this.BTN_AC8);
            this.groupBox_Det_Venta.Controls.Add(this.JLB_codprd_DET);
            this.groupBox_Det_Venta.Controls.Add(this.JLB_NVENTA);
            this.groupBox_Det_Venta.Controls.Add(this.JLB_DET_CANT_VENT);
            this.groupBox_Det_Venta.Controls.Add(this.JLB_Det_venta);
            this.groupBox_Det_Venta.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox_Det_Venta.Location = new System.Drawing.Point(6, 12);
            this.groupBox_Det_Venta.Name = "groupBox_Det_Venta";
            this.groupBox_Det_Venta.Size = new System.Drawing.Size(1062, 223);
            this.groupBox_Det_Venta.TabIndex = 3;
            this.groupBox_Det_Venta.TabStop = false;
            this.groupBox_Det_Venta.Text = "Datos De Los Detalles De Las Ventas";
            // 
            // SUBTO_DET_VENT
            // 
            this.SUBTO_DET_VENT.Location = new System.Drawing.Point(722, 110);
            this.SUBTO_DET_VENT.Name = "SUBTO_DET_VENT";
            this.SUBTO_DET_VENT.Size = new System.Drawing.Size(208, 26);
            this.SUBTO_DET_VENT.TabIndex = 14;
            // 
            // JLBSUB_VENT
            // 
            this.JLBSUB_VENT.AutoSize = true;
            this.JLBSUB_VENT.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLBSUB_VENT.Location = new System.Drawing.Point(625, 112);
            this.JLBSUB_VENT.Name = "JLBSUB_VENT";
            this.JLBSUB_VENT.Size = new System.Drawing.Size(82, 19);
            this.JLBSUB_VENT.TabIndex = 13;
            this.JLBSUB_VENT.Text = "Sub Total:";
            // 
            // CANT_DET_VENTA
            // 
            this.CANT_DET_VENTA.Location = new System.Drawing.Point(722, 49);
            this.CANT_DET_VENTA.Name = "CANT_DET_VENTA";
            this.CANT_DET_VENTA.Size = new System.Drawing.Size(208, 26);
            this.CANT_DET_VENTA.TabIndex = 11;
            // 
            // COD_PROD_DET_VENT
            // 
            this.COD_PROD_DET_VENT.Location = new System.Drawing.Point(276, 166);
            this.COD_PROD_DET_VENT.Name = "COD_PROD_DET_VENT";
            this.COD_PROD_DET_VENT.Size = new System.Drawing.Size(147, 26);
            this.COD_PROD_DET_VENT.TabIndex = 10;
            // 
            // NVENTA_DET_VENT
            // 
            this.NVENTA_DET_VENT.Location = new System.Drawing.Point(276, 105);
            this.NVENTA_DET_VENT.Name = "NVENTA_DET_VENT";
            this.NVENTA_DET_VENT.Size = new System.Drawing.Size(249, 26);
            this.NVENTA_DET_VENT.TabIndex = 9;
            // 
            // ID_DET_VENT
            // 
            this.ID_DET_VENT.Location = new System.Drawing.Point(276, 42);
            this.ID_DET_VENT.Name = "ID_DET_VENT";
            this.ID_DET_VENT.Size = new System.Drawing.Size(249, 26);
            this.ID_DET_VENT.TabIndex = 8;
            // 
            // BTN_AG8
            // 
            this.BTN_AG8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.BTN_AG8.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.BTN_AG8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.BTN_AG8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.BTN_AG8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_AG8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_AG8.ForeColor = System.Drawing.Color.White;
            this.BTN_AG8.Location = new System.Drawing.Point(896, 191);
            this.BTN_AG8.Name = "BTN_AG8";
            this.BTN_AG8.Size = new System.Drawing.Size(75, 23);
            this.BTN_AG8.TabIndex = 7;
            this.BTN_AG8.Text = "Agregar";
            this.BTN_AG8.UseVisualStyleBackColor = false;
            // 
            // BTN_ELM8
            // 
            this.BTN_ELM8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BTN_ELM8.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.BTN_ELM8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BTN_ELM8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BTN_ELM8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_ELM8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_ELM8.ForeColor = System.Drawing.Color.White;
            this.BTN_ELM8.Location = new System.Drawing.Point(797, 191);
            this.BTN_ELM8.Name = "BTN_ELM8";
            this.BTN_ELM8.Size = new System.Drawing.Size(75, 23);
            this.BTN_ELM8.TabIndex = 6;
            this.BTN_ELM8.Text = "Eliminar";
            this.BTN_ELM8.UseVisualStyleBackColor = false;
            // 
            // BTN_AC8
            // 
            this.BTN_AC8.BackColor = System.Drawing.Color.DarkBlue;
            this.BTN_AC8.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.BTN_AC8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkBlue;
            this.BTN_AC8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue;
            this.BTN_AC8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_AC8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_AC8.ForeColor = System.Drawing.Color.White;
            this.BTN_AC8.Location = new System.Drawing.Point(691, 191);
            this.BTN_AC8.Name = "BTN_AC8";
            this.BTN_AC8.Size = new System.Drawing.Size(75, 23);
            this.BTN_AC8.TabIndex = 5;
            this.BTN_AC8.Text = "Actualizar";
            this.BTN_AC8.UseVisualStyleBackColor = false;
            // 
            // JLB_codprd_DET
            // 
            this.JLB_codprd_DET.AutoSize = true;
            this.JLB_codprd_DET.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_codprd_DET.Location = new System.Drawing.Point(88, 168);
            this.JLB_codprd_DET.Name = "JLB_codprd_DET";
            this.JLB_codprd_DET.Size = new System.Drawing.Size(173, 19);
            this.JLB_codprd_DET.TabIndex = 4;
            this.JLB_codprd_DET.Text = "Código Del Producto:";
            // 
            // JLB_NVENTA
            // 
            this.JLB_NVENTA.AutoSize = true;
            this.JLB_NVENTA.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_NVENTA.Location = new System.Drawing.Point(88, 110);
            this.JLB_NVENTA.Name = "JLB_NVENTA";
            this.JLB_NVENTA.Size = new System.Drawing.Size(173, 19);
            this.JLB_NVENTA.TabIndex = 2;
            this.JLB_NVENTA.Text = "Número De La Venta:";
            // 
            // JLB_DET_CANT_VENT
            // 
            this.JLB_DET_CANT_VENT.AutoSize = true;
            this.JLB_DET_CANT_VENT.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_DET_CANT_VENT.Location = new System.Drawing.Point(620, 51);
            this.JLB_DET_CANT_VENT.Name = "JLB_DET_CANT_VENT";
            this.JLB_DET_CANT_VENT.Size = new System.Drawing.Size(87, 19);
            this.JLB_DET_CANT_VENT.TabIndex = 1;
            this.JLB_DET_CANT_VENT.Text = "Cantidad:";
            // 
            // JLB_Det_venta
            // 
            this.JLB_Det_venta.AutoSize = true;
            this.JLB_Det_venta.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_Det_venta.Location = new System.Drawing.Point(125, 49);
            this.JLB_Det_venta.Name = "JLB_Det_venta";
            this.JLB_Det_venta.Size = new System.Drawing.Size(136, 19);
            this.JLB_Det_venta.TabIndex = 0;
            this.JLB_Det_venta.Text = "ID Detalle Venta:";
            // 
            // DetalleVentas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1080, 615);
            this.Controls.Add(this.groupBox_Det_Venta);
            this.Controls.Add(this.panelDetVenta);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "DetalleVentas";
            this.Text = "DetalleVentas";
            this.groupBox_Det_Venta.ResumeLayout(false);
            this.groupBox_Det_Venta.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panelDetVenta;
        private System.Windows.Forms.GroupBox groupBox_Det_Venta;
        private System.Windows.Forms.TextBox SUBTO_DET_VENT;
        private System.Windows.Forms.Label JLBSUB_VENT;
        private System.Windows.Forms.TextBox CANT_DET_VENTA;
        private System.Windows.Forms.TextBox COD_PROD_DET_VENT;
        private System.Windows.Forms.TextBox NVENTA_DET_VENT;
        private System.Windows.Forms.TextBox ID_DET_VENT;
        private System.Windows.Forms.Button BTN_AG8;
        private System.Windows.Forms.Button BTN_ELM8;
        private System.Windows.Forms.Button BTN_AC8;
        private System.Windows.Forms.Label JLB_codprd_DET;
        private System.Windows.Forms.Label JLB_NVENTA;
        private System.Windows.Forms.Label JLB_DET_CANT_VENT;
        private System.Windows.Forms.Label JLB_Det_venta;
    }
}