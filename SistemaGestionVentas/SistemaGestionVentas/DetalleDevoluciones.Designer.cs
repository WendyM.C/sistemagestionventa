﻿
namespace SistemaGestionVentas
{
    partial class DetalleDevoluciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox_Det_Dev = new System.Windows.Forms.GroupBox();
            this.SUBTO_DET_DEV = new System.Windows.Forms.TextBox();
            this.JLBSUBT_DEV = new System.Windows.Forms.Label();
            this.CANT_DET_DEV = new System.Windows.Forms.TextBox();
            this.COD_PROD_DET_DEV = new System.Windows.Forms.TextBox();
            this.IDEVOL_DET_DEV = new System.Windows.Forms.TextBox();
            this.ID_DET_DEV = new System.Windows.Forms.TextBox();
            this.BTN_AG8 = new System.Windows.Forms.Button();
            this.BTN_ELM8 = new System.Windows.Forms.Button();
            this.BTN_AC8 = new System.Windows.Forms.Button();
            this.JLB_codprd_DET = new System.Windows.Forms.Label();
            this.JLB_iddev = new System.Windows.Forms.Label();
            this.JLB_DET_CANT_DEV = new System.Windows.Forms.Label();
            this.JLB_Det_Devolucion = new System.Windows.Forms.Label();
            this.panelDetDev = new System.Windows.Forms.Panel();
            this.JLB_MOT_DEV = new System.Windows.Forms.Label();
            this.Motivo_DEV = new System.Windows.Forms.TextBox();
            this.groupBox_Det_Dev.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox_Det_Dev
            // 
            this.groupBox_Det_Dev.BackColor = System.Drawing.Color.LightSteelBlue;
            this.groupBox_Det_Dev.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groupBox_Det_Dev.Controls.Add(this.Motivo_DEV);
            this.groupBox_Det_Dev.Controls.Add(this.JLB_MOT_DEV);
            this.groupBox_Det_Dev.Controls.Add(this.SUBTO_DET_DEV);
            this.groupBox_Det_Dev.Controls.Add(this.JLBSUBT_DEV);
            this.groupBox_Det_Dev.Controls.Add(this.CANT_DET_DEV);
            this.groupBox_Det_Dev.Controls.Add(this.COD_PROD_DET_DEV);
            this.groupBox_Det_Dev.Controls.Add(this.IDEVOL_DET_DEV);
            this.groupBox_Det_Dev.Controls.Add(this.ID_DET_DEV);
            this.groupBox_Det_Dev.Controls.Add(this.BTN_AG8);
            this.groupBox_Det_Dev.Controls.Add(this.BTN_ELM8);
            this.groupBox_Det_Dev.Controls.Add(this.BTN_AC8);
            this.groupBox_Det_Dev.Controls.Add(this.JLB_codprd_DET);
            this.groupBox_Det_Dev.Controls.Add(this.JLB_iddev);
            this.groupBox_Det_Dev.Controls.Add(this.JLB_DET_CANT_DEV);
            this.groupBox_Det_Dev.Controls.Add(this.JLB_Det_Devolucion);
            this.groupBox_Det_Dev.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox_Det_Dev.Location = new System.Drawing.Point(6, 12);
            this.groupBox_Det_Dev.Name = "groupBox_Det_Dev";
            this.groupBox_Det_Dev.Size = new System.Drawing.Size(1062, 223);
            this.groupBox_Det_Dev.TabIndex = 4;
            this.groupBox_Det_Dev.TabStop = false;
            this.groupBox_Det_Dev.Text = "Datos De Los Detalles De Las Devoluciones";
            // 
            // SUBTO_DET_DEV
            // 
            this.SUBTO_DET_DEV.Location = new System.Drawing.Point(881, 110);
            this.SUBTO_DET_DEV.Name = "SUBTO_DET_DEV";
            this.SUBTO_DET_DEV.Size = new System.Drawing.Size(90, 26);
            this.SUBTO_DET_DEV.TabIndex = 14;
            // 
            // JLBSUBT_DEV
            // 
            this.JLBSUBT_DEV.AutoSize = true;
            this.JLBSUBT_DEV.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLBSUBT_DEV.Location = new System.Drawing.Point(795, 112);
            this.JLBSUBT_DEV.Name = "JLBSUBT_DEV";
            this.JLBSUBT_DEV.Size = new System.Drawing.Size(82, 19);
            this.JLBSUBT_DEV.TabIndex = 13;
            this.JLBSUBT_DEV.Text = "Sub Total:";
            // 
            // CANT_DET_DEV
            // 
            this.CANT_DET_DEV.Location = new System.Drawing.Point(683, 110);
            this.CANT_DET_DEV.Name = "CANT_DET_DEV";
            this.CANT_DET_DEV.Size = new System.Drawing.Size(100, 26);
            this.CANT_DET_DEV.TabIndex = 11;
            // 
            // COD_PROD_DET_DEV
            // 
            this.COD_PROD_DET_DEV.Location = new System.Drawing.Point(295, 164);
            this.COD_PROD_DET_DEV.Name = "COD_PROD_DET_DEV";
            this.COD_PROD_DET_DEV.Size = new System.Drawing.Size(147, 26);
            this.COD_PROD_DET_DEV.TabIndex = 10;
            // 
            // IDEVOL_DET_DEV
            // 
            this.IDEVOL_DET_DEV.Location = new System.Drawing.Point(295, 110);
            this.IDEVOL_DET_DEV.Name = "IDEVOL_DET_DEV";
            this.IDEVOL_DET_DEV.Size = new System.Drawing.Size(249, 26);
            this.IDEVOL_DET_DEV.TabIndex = 9;
            // 
            // ID_DET_DEV
            // 
            this.ID_DET_DEV.Location = new System.Drawing.Point(295, 51);
            this.ID_DET_DEV.Name = "ID_DET_DEV";
            this.ID_DET_DEV.Size = new System.Drawing.Size(249, 26);
            this.ID_DET_DEV.TabIndex = 8;
            // 
            // BTN_AG8
            // 
            this.BTN_AG8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.BTN_AG8.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.BTN_AG8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.BTN_AG8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.BTN_AG8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_AG8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_AG8.ForeColor = System.Drawing.Color.White;
            this.BTN_AG8.Location = new System.Drawing.Point(896, 191);
            this.BTN_AG8.Name = "BTN_AG8";
            this.BTN_AG8.Size = new System.Drawing.Size(75, 23);
            this.BTN_AG8.TabIndex = 7;
            this.BTN_AG8.Text = "Agregar";
            this.BTN_AG8.UseVisualStyleBackColor = false;
            // 
            // BTN_ELM8
            // 
            this.BTN_ELM8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BTN_ELM8.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.BTN_ELM8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BTN_ELM8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BTN_ELM8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_ELM8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_ELM8.ForeColor = System.Drawing.Color.White;
            this.BTN_ELM8.Location = new System.Drawing.Point(797, 191);
            this.BTN_ELM8.Name = "BTN_ELM8";
            this.BTN_ELM8.Size = new System.Drawing.Size(75, 23);
            this.BTN_ELM8.TabIndex = 6;
            this.BTN_ELM8.Text = "Eliminar";
            this.BTN_ELM8.UseVisualStyleBackColor = false;
            // 
            // BTN_AC8
            // 
            this.BTN_AC8.BackColor = System.Drawing.Color.DarkBlue;
            this.BTN_AC8.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.BTN_AC8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkBlue;
            this.BTN_AC8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue;
            this.BTN_AC8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_AC8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_AC8.ForeColor = System.Drawing.Color.White;
            this.BTN_AC8.Location = new System.Drawing.Point(691, 191);
            this.BTN_AC8.Name = "BTN_AC8";
            this.BTN_AC8.Size = new System.Drawing.Size(75, 23);
            this.BTN_AC8.TabIndex = 5;
            this.BTN_AC8.Text = "Actualizar";
            this.BTN_AC8.UseVisualStyleBackColor = false;
            // 
            // JLB_codprd_DET
            // 
            this.JLB_codprd_DET.AutoSize = true;
            this.JLB_codprd_DET.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_codprd_DET.Location = new System.Drawing.Point(103, 166);
            this.JLB_codprd_DET.Name = "JLB_codprd_DET";
            this.JLB_codprd_DET.Size = new System.Drawing.Size(173, 19);
            this.JLB_codprd_DET.TabIndex = 4;
            this.JLB_codprd_DET.Text = "Código Del Producto:";
            // 
            // JLB_iddev
            // 
            this.JLB_iddev.AutoSize = true;
            this.JLB_iddev.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_iddev.Location = new System.Drawing.Point(156, 112);
            this.JLB_iddev.Name = "JLB_iddev";
            this.JLB_iddev.Size = new System.Drawing.Size(120, 19);
            this.JLB_iddev.TabIndex = 2;
            this.JLB_iddev.Text = "ID Devolucion:";
            // 
            // JLB_DET_CANT_DEV
            // 
            this.JLB_DET_CANT_DEV.AutoSize = true;
            this.JLB_DET_CANT_DEV.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_DET_CANT_DEV.Location = new System.Drawing.Point(593, 112);
            this.JLB_DET_CANT_DEV.Name = "JLB_DET_CANT_DEV";
            this.JLB_DET_CANT_DEV.Size = new System.Drawing.Size(87, 19);
            this.JLB_DET_CANT_DEV.TabIndex = 1;
            this.JLB_DET_CANT_DEV.Text = "Cantidad:";
            // 
            // JLB_Det_Devolucion
            // 
            this.JLB_Det_Devolucion.AutoSize = true;
            this.JLB_Det_Devolucion.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_Det_Devolucion.Location = new System.Drawing.Point(98, 58);
            this.JLB_Det_Devolucion.Name = "JLB_Det_Devolucion";
            this.JLB_Det_Devolucion.Size = new System.Drawing.Size(178, 19);
            this.JLB_Det_Devolucion.TabIndex = 0;
            this.JLB_Det_Devolucion.Text = "ID Detalle Devolución:";
            // 
            // panelDetDev
            // 
            this.panelDetDev.BackColor = System.Drawing.Color.LightGray;
            this.panelDetDev.Location = new System.Drawing.Point(6, 241);
            this.panelDetDev.Name = "panelDetDev";
            this.panelDetDev.Size = new System.Drawing.Size(1062, 362);
            this.panelDetDev.TabIndex = 5;
            // 
            // JLB_MOT_DEV
            // 
            this.JLB_MOT_DEV.AutoSize = true;
            this.JLB_MOT_DEV.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_MOT_DEV.Location = new System.Drawing.Point(612, 58);
            this.JLB_MOT_DEV.Name = "JLB_MOT_DEV";
            this.JLB_MOT_DEV.Size = new System.Drawing.Size(64, 19);
            this.JLB_MOT_DEV.TabIndex = 15;
            this.JLB_MOT_DEV.Text = "Motivo:";
            // 
            // Motivo_DEV
            // 
            this.Motivo_DEV.Location = new System.Drawing.Point(683, 51);
            this.Motivo_DEV.Multiline = true;
            this.Motivo_DEV.Name = "Motivo_DEV";
            this.Motivo_DEV.Size = new System.Drawing.Size(288, 43);
            this.Motivo_DEV.TabIndex = 16;
            // 
            // DetalleDevoluciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1080, 615);
            this.Controls.Add(this.panelDetDev);
            this.Controls.Add(this.groupBox_Det_Dev);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "DetalleDevoluciones";
            this.Text = "DetalleDevoluciones";
            this.groupBox_Det_Dev.ResumeLayout(false);
            this.groupBox_Det_Dev.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox_Det_Dev;
        private System.Windows.Forms.TextBox SUBTO_DET_DEV;
        private System.Windows.Forms.Label JLBSUBT_DEV;
        private System.Windows.Forms.TextBox CANT_DET_DEV;
        private System.Windows.Forms.TextBox COD_PROD_DET_DEV;
        private System.Windows.Forms.TextBox IDEVOL_DET_DEV;
        private System.Windows.Forms.TextBox ID_DET_DEV;
        private System.Windows.Forms.Button BTN_AG8;
        private System.Windows.Forms.Button BTN_ELM8;
        private System.Windows.Forms.Button BTN_AC8;
        private System.Windows.Forms.Label JLB_codprd_DET;
        private System.Windows.Forms.Label JLB_iddev;
        private System.Windows.Forms.Label JLB_DET_CANT_DEV;
        private System.Windows.Forms.Label JLB_Det_Devolucion;
        private System.Windows.Forms.Panel panelDetDev;
        private System.Windows.Forms.TextBox Motivo_DEV;
        private System.Windows.Forms.Label JLB_MOT_DEV;
    }
}