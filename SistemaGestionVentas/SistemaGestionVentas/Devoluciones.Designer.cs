﻿
namespace SistemaGestionVentas
{
    partial class Devoluciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox_Dev = new System.Windows.Forms.GroupBox();
            this.FECHADEV = new System.Windows.Forms.TextBox();
            this.TOTALDEV = new System.Windows.Forms.TextBox();
            this.NVENTA = new System.Windows.Forms.TextBox();
            this.IDCOMP = new System.Windows.Forms.TextBox();
            this.IDDEV = new System.Windows.Forms.TextBox();
            this.BTN_AG5 = new System.Windows.Forms.Button();
            this.BTN_ELM5 = new System.Windows.Forms.Button();
            this.BTN_AC5 = new System.Windows.Forms.Button();
            this.JLB_NVENTA = new System.Windows.Forms.Label();
            this.JLB_fechaDEV = new System.Windows.Forms.Label();
            this.JLB_idcompra = new System.Windows.Forms.Label();
            this.JLB_TOTALDEV = new System.Windows.Forms.Label();
            this.JLB_Dev = new System.Windows.Forms.Label();
            this.panelDevl = new System.Windows.Forms.Panel();
            this.groupBox_Dev.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox_Dev
            // 
            this.groupBox_Dev.BackColor = System.Drawing.Color.LightSteelBlue;
            this.groupBox_Dev.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groupBox_Dev.Controls.Add(this.FECHADEV);
            this.groupBox_Dev.Controls.Add(this.TOTALDEV);
            this.groupBox_Dev.Controls.Add(this.NVENTA);
            this.groupBox_Dev.Controls.Add(this.IDCOMP);
            this.groupBox_Dev.Controls.Add(this.IDDEV);
            this.groupBox_Dev.Controls.Add(this.BTN_AG5);
            this.groupBox_Dev.Controls.Add(this.BTN_ELM5);
            this.groupBox_Dev.Controls.Add(this.BTN_AC5);
            this.groupBox_Dev.Controls.Add(this.JLB_NVENTA);
            this.groupBox_Dev.Controls.Add(this.JLB_fechaDEV);
            this.groupBox_Dev.Controls.Add(this.JLB_idcompra);
            this.groupBox_Dev.Controls.Add(this.JLB_TOTALDEV);
            this.groupBox_Dev.Controls.Add(this.JLB_Dev);
            this.groupBox_Dev.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox_Dev.Location = new System.Drawing.Point(6, 12);
            this.groupBox_Dev.Name = "groupBox_Dev";
            this.groupBox_Dev.Size = new System.Drawing.Size(1062, 223);
            this.groupBox_Dev.TabIndex = 1;
            this.groupBox_Dev.TabStop = false;
            this.groupBox_Dev.Text = "Datos De Las Devoluciones";
            // 
            // FECHADEV
            // 
            this.FECHADEV.Location = new System.Drawing.Point(712, 108);
            this.FECHADEV.Name = "FECHADEV";
            this.FECHADEV.Size = new System.Drawing.Size(233, 26);
            this.FECHADEV.TabIndex = 12;
            // 
            // TOTALDEV
            // 
            this.TOTALDEV.Location = new System.Drawing.Point(712, 54);
            this.TOTALDEV.Name = "TOTALDEV";
            this.TOTALDEV.Size = new System.Drawing.Size(233, 26);
            this.TOTALDEV.TabIndex = 11;
            // 
            // NVENTA
            // 
            this.NVENTA.Location = new System.Drawing.Point(255, 175);
            this.NVENTA.Name = "NVENTA";
            this.NVENTA.Size = new System.Drawing.Size(144, 26);
            this.NVENTA.TabIndex = 10;
            // 
            // IDCOMP
            // 
            this.IDCOMP.Location = new System.Drawing.Point(255, 115);
            this.IDCOMP.Name = "IDCOMP";
            this.IDCOMP.Size = new System.Drawing.Size(225, 26);
            this.IDCOMP.TabIndex = 9;
            // 
            // IDDEV
            // 
            this.IDDEV.Location = new System.Drawing.Point(255, 54);
            this.IDDEV.Name = "IDDEV";
            this.IDDEV.Size = new System.Drawing.Size(225, 26);
            this.IDDEV.TabIndex = 8;
            // 
            // BTN_AG5
            // 
            this.BTN_AG5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.BTN_AG5.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.BTN_AG5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.BTN_AG5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.BTN_AG5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_AG5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_AG5.ForeColor = System.Drawing.Color.White;
            this.BTN_AG5.Location = new System.Drawing.Point(896, 191);
            this.BTN_AG5.Name = "BTN_AG5";
            this.BTN_AG5.Size = new System.Drawing.Size(75, 23);
            this.BTN_AG5.TabIndex = 7;
            this.BTN_AG5.Text = "Agregar";
            this.BTN_AG5.UseVisualStyleBackColor = false;
            // 
            // BTN_ELM5
            // 
            this.BTN_ELM5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BTN_ELM5.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.BTN_ELM5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BTN_ELM5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BTN_ELM5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_ELM5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_ELM5.ForeColor = System.Drawing.Color.White;
            this.BTN_ELM5.Location = new System.Drawing.Point(797, 191);
            this.BTN_ELM5.Name = "BTN_ELM5";
            this.BTN_ELM5.Size = new System.Drawing.Size(75, 23);
            this.BTN_ELM5.TabIndex = 6;
            this.BTN_ELM5.Text = "Eliminar";
            this.BTN_ELM5.UseVisualStyleBackColor = false;
            // 
            // BTN_AC5
            // 
            this.BTN_AC5.BackColor = System.Drawing.Color.DarkBlue;
            this.BTN_AC5.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.BTN_AC5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkBlue;
            this.BTN_AC5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue;
            this.BTN_AC5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_AC5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_AC5.ForeColor = System.Drawing.Color.White;
            this.BTN_AC5.Location = new System.Drawing.Point(691, 191);
            this.BTN_AC5.Name = "BTN_AC5";
            this.BTN_AC5.Size = new System.Drawing.Size(75, 23);
            this.BTN_AC5.TabIndex = 5;
            this.BTN_AC5.Text = "Actualizar";
            this.BTN_AC5.UseVisualStyleBackColor = false;
            // 
            // JLB_NVENTA
            // 
            this.JLB_NVENTA.AutoSize = true;
            this.JLB_NVENTA.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_NVENTA.Location = new System.Drawing.Point(79, 175);
            this.JLB_NVENTA.Name = "JLB_NVENTA";
            this.JLB_NVENTA.Size = new System.Drawing.Size(170, 19);
            this.JLB_NVENTA.TabIndex = 4;
            this.JLB_NVENTA.Text = "Numero de la Venta:";
            // 
            // JLB_fechaDEV
            // 
            this.JLB_fechaDEV.AutoSize = true;
            this.JLB_fechaDEV.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_fechaDEV.Location = new System.Drawing.Point(552, 115);
            this.JLB_fechaDEV.Name = "JLB_fechaDEV";
            this.JLB_fechaDEV.Size = new System.Drawing.Size(154, 19);
            this.JLB_fechaDEV.TabIndex = 3;
            this.JLB_fechaDEV.Text = "Fecha Devolucion:";
            // 
            // JLB_idcompra
            // 
            this.JLB_idcompra.AutoSize = true;
            this.JLB_idcompra.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_idcompra.Location = new System.Drawing.Point(152, 115);
            this.JLB_idcompra.Name = "JLB_idcompra";
            this.JLB_idcompra.Size = new System.Drawing.Size(97, 19);
            this.JLB_idcompra.TabIndex = 2;
            this.JLB_idcompra.Text = "ID Compra:";
            // 
            // JLB_TOTALDEV
            // 
            this.JLB_TOTALDEV.AutoSize = true;
            this.JLB_TOTALDEV.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_TOTALDEV.Location = new System.Drawing.Point(565, 56);
            this.JLB_TOTALDEV.Name = "JLB_TOTALDEV";
            this.JLB_TOTALDEV.Size = new System.Drawing.Size(141, 19);
            this.JLB_TOTALDEV.TabIndex = 1;
            this.JLB_TOTALDEV.Text = "Total Devolucion:";
            // 
            // JLB_Dev
            // 
            this.JLB_Dev.AutoSize = true;
            this.JLB_Dev.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JLB_Dev.Location = new System.Drawing.Point(129, 56);
            this.JLB_Dev.Name = "JLB_Dev";
            this.JLB_Dev.Size = new System.Drawing.Size(120, 19);
            this.JLB_Dev.TabIndex = 0;
            this.JLB_Dev.Text = "ID Devolucion:";
            // 
            // panelDevl
            // 
            this.panelDevl.BackColor = System.Drawing.Color.LightGray;
            this.panelDevl.Location = new System.Drawing.Point(6, 241);
            this.panelDevl.Name = "panelDevl";
            this.panelDevl.Size = new System.Drawing.Size(1062, 362);
            this.panelDevl.TabIndex = 2;
            // 
            // Devoluciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1080, 615);
            this.Controls.Add(this.panelDevl);
            this.Controls.Add(this.groupBox_Dev);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Devoluciones";
            this.Text = "Devoluciones";
            this.groupBox_Dev.ResumeLayout(false);
            this.groupBox_Dev.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox_Dev;
        private System.Windows.Forms.TextBox FECHADEV;
        private System.Windows.Forms.TextBox TOTALDEV;
        private System.Windows.Forms.TextBox NVENTA;
        private System.Windows.Forms.TextBox IDCOMP;
        private System.Windows.Forms.TextBox IDDEV;
        private System.Windows.Forms.Button BTN_AG5;
        private System.Windows.Forms.Button BTN_ELM5;
        private System.Windows.Forms.Button BTN_AC5;
        private System.Windows.Forms.Label JLB_NVENTA;
        private System.Windows.Forms.Label JLB_fechaDEV;
        private System.Windows.Forms.Label JLB_idcompra;
        private System.Windows.Forms.Label JLB_TOTALDEV;
        private System.Windows.Forms.Label JLB_Dev;
        private System.Windows.Forms.Panel panelDevl;
    }
}