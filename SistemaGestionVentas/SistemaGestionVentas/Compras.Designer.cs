﻿
namespace SistemaGestionVentas
{
    partial class Compras
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox_Compras = new System.Windows.Forms.GroupBox();
            this.BTN_AG4 = new System.Windows.Forms.Button();
            this.BTN_EL4 = new System.Windows.Forms.Button();
            this.BTN_AC4 = new System.Windows.Forms.Button();
            this.TIPOPAG = new System.Windows.Forms.TextBox();
            this.IDProv = new System.Windows.Forms.TextBox();
            this.FechaCom = new System.Windows.Forms.TextBox();
            this.TotalCOM = new System.Windows.Forms.TextBox();
            this.IDCom = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panelCompra = new System.Windows.Forms.Panel();
            this.groupBox_Compras.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox_Compras
            // 
            this.groupBox_Compras.BackColor = System.Drawing.Color.LightSteelBlue;
            this.groupBox_Compras.Controls.Add(this.BTN_AG4);
            this.groupBox_Compras.Controls.Add(this.BTN_EL4);
            this.groupBox_Compras.Controls.Add(this.BTN_AC4);
            this.groupBox_Compras.Controls.Add(this.TIPOPAG);
            this.groupBox_Compras.Controls.Add(this.IDProv);
            this.groupBox_Compras.Controls.Add(this.FechaCom);
            this.groupBox_Compras.Controls.Add(this.TotalCOM);
            this.groupBox_Compras.Controls.Add(this.IDCom);
            this.groupBox_Compras.Controls.Add(this.label5);
            this.groupBox_Compras.Controls.Add(this.label4);
            this.groupBox_Compras.Controls.Add(this.label3);
            this.groupBox_Compras.Controls.Add(this.label2);
            this.groupBox_Compras.Controls.Add(this.label1);
            this.groupBox_Compras.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox_Compras.Location = new System.Drawing.Point(6, 12);
            this.groupBox_Compras.Name = "groupBox_Compras";
            this.groupBox_Compras.Size = new System.Drawing.Size(1062, 223);
            this.groupBox_Compras.TabIndex = 0;
            this.groupBox_Compras.TabStop = false;
            this.groupBox_Compras.Text = "Datos De Las Compras";
            // 
            // BTN_AG4
            // 
            this.BTN_AG4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.BTN_AG4.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.BTN_AG4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.BTN_AG4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.BTN_AG4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_AG4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_AG4.ForeColor = System.Drawing.Color.White;
            this.BTN_AG4.Location = new System.Drawing.Point(896, 191);
            this.BTN_AG4.Name = "BTN_AG4";
            this.BTN_AG4.Size = new System.Drawing.Size(75, 23);
            this.BTN_AG4.TabIndex = 12;
            this.BTN_AG4.Text = "Agregar";
            this.BTN_AG4.UseVisualStyleBackColor = false;
            // 
            // BTN_EL4
            // 
            this.BTN_EL4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BTN_EL4.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.BTN_EL4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BTN_EL4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BTN_EL4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_EL4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_EL4.ForeColor = System.Drawing.Color.White;
            this.BTN_EL4.Location = new System.Drawing.Point(797, 191);
            this.BTN_EL4.Name = "BTN_EL4";
            this.BTN_EL4.Size = new System.Drawing.Size(75, 23);
            this.BTN_EL4.TabIndex = 11;
            this.BTN_EL4.Text = "Eliminar";
            this.BTN_EL4.UseVisualStyleBackColor = false;
            // 
            // BTN_AC4
            // 
            this.BTN_AC4.BackColor = System.Drawing.Color.DarkBlue;
            this.BTN_AC4.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.BTN_AC4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkBlue;
            this.BTN_AC4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DodgerBlue;
            this.BTN_AC4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_AC4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_AC4.ForeColor = System.Drawing.Color.White;
            this.BTN_AC4.Location = new System.Drawing.Point(691, 191);
            this.BTN_AC4.Name = "BTN_AC4";
            this.BTN_AC4.Size = new System.Drawing.Size(75, 23);
            this.BTN_AC4.TabIndex = 10;
            this.BTN_AC4.Text = "Actualizar";
            this.BTN_AC4.UseVisualStyleBackColor = false;
            // 
            // TIPOPAG
            // 
            this.TIPOPAG.Location = new System.Drawing.Point(718, 108);
            this.TIPOPAG.Name = "TIPOPAG";
            this.TIPOPAG.Size = new System.Drawing.Size(173, 26);
            this.TIPOPAG.TabIndex = 9;
            // 
            // IDProv
            // 
            this.IDProv.Location = new System.Drawing.Point(243, 169);
            this.IDProv.Name = "IDProv";
            this.IDProv.Size = new System.Drawing.Size(181, 26);
            this.IDProv.TabIndex = 8;
            // 
            // FechaCom
            // 
            this.FechaCom.Location = new System.Drawing.Point(243, 109);
            this.FechaCom.Name = "FechaCom";
            this.FechaCom.Size = new System.Drawing.Size(181, 26);
            this.FechaCom.TabIndex = 7;
            // 
            // TotalCOM
            // 
            this.TotalCOM.Location = new System.Drawing.Point(725, 52);
            this.TotalCOM.Name = "TotalCOM";
            this.TotalCOM.Size = new System.Drawing.Size(173, 26);
            this.TotalCOM.TabIndex = 6;
            // 
            // IDCom
            // 
            this.IDCom.Location = new System.Drawing.Point(243, 55);
            this.IDCom.Name = "IDCom";
            this.IDCom.Size = new System.Drawing.Size(181, 26);
            this.IDCom.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(601, 111);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(115, 19);
            this.label5.TabIndex = 4;
            this.label5.Text = "Tipo de Pago:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(601, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(118, 19);
            this.label4.TabIndex = 3;
            this.label4.Text = "Total Compra:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(125, 171);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 19);
            this.label3.TabIndex = 2;
            this.label3.Text = "ID Proveedor:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(61, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(175, 19);
            this.label2.TabIndex = 1;
            this.label2.Text = "Fecha de la Compra:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(140, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID Compra:";
            // 
            // panelCompra
            // 
            this.panelCompra.BackColor = System.Drawing.Color.LightGray;
            this.panelCompra.Location = new System.Drawing.Point(6, 241);
            this.panelCompra.Name = "panelCompra";
            this.panelCompra.Size = new System.Drawing.Size(1062, 362);
            this.panelCompra.TabIndex = 1;
            // 
            // Compras
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1080, 615);
            this.Controls.Add(this.panelCompra);
            this.Controls.Add(this.groupBox_Compras);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Compras";
            this.Text = "Compras";
            this.groupBox_Compras.ResumeLayout(false);
            this.groupBox_Compras.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox_Compras;
        private System.Windows.Forms.Panel panelCompra;
        private System.Windows.Forms.Button BTN_AG4;
        private System.Windows.Forms.Button BTN_EL4;
        private System.Windows.Forms.Button BTN_AC4;
        private System.Windows.Forms.TextBox TIPOPAG;
        private System.Windows.Forms.TextBox IDProv;
        private System.Windows.Forms.TextBox FechaCom;
        private System.Windows.Forms.TextBox TotalCOM;
        private System.Windows.Forms.TextBox IDCom;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}