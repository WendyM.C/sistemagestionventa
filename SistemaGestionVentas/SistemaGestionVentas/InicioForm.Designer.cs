﻿
namespace SistemaGestionVentas
{
    partial class InicioForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InicioForm));
            this.BarraTitulo = new System.Windows.Forms.Panel();
            this.MenuVertical = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.Sub_menuRep = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.BTN_producto = new System.Windows.Forms.Button();
            this.PanelContenedor = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.BTN_Cerrarsesion = new System.Windows.Forms.PictureBox();
            this.Reporte = new System.Windows.Forms.Button();
            this.Det_Devolucion = new System.Windows.Forms.Button();
            this.Det_ventas = new System.Windows.Forms.Button();
            this.Det_com = new System.Windows.Forms.Button();
            this.Tipo_Producto = new System.Windows.Forms.Button();
            this.Cliente = new System.Windows.Forms.Button();
            this.venta = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.Compra = new System.Windows.Forms.Button();
            this.devoluciones = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.BTNcerrar_inicio = new System.Windows.Forms.PictureBox();
            this.BTNminimizar_inicio = new System.Windows.Forms.PictureBox();
            this.BTNMaximizar_inicio = new System.Windows.Forms.PictureBox();
            this.BTNRestaurar = new System.Windows.Forms.PictureBox();
            this.BarraTitulo.SuspendLayout();
            this.MenuVertical.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.Sub_menuRep.SuspendLayout();
            this.PanelContenedor.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BTN_Cerrarsesion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTNcerrar_inicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTNminimizar_inicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTNMaximizar_inicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTNRestaurar)).BeginInit();
            this.SuspendLayout();
            // 
            // BarraTitulo
            // 
            this.BarraTitulo.BackColor = System.Drawing.Color.DarkBlue;
            this.BarraTitulo.Controls.Add(this.BTNcerrar_inicio);
            this.BarraTitulo.Controls.Add(this.BTNminimizar_inicio);
            this.BarraTitulo.Controls.Add(this.BTNMaximizar_inicio);
            this.BarraTitulo.Controls.Add(this.BTNRestaurar);
            this.BarraTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.BarraTitulo.Location = new System.Drawing.Point(0, 0);
            this.BarraTitulo.Name = "BarraTitulo";
            this.BarraTitulo.Size = new System.Drawing.Size(1300, 35);
            this.BarraTitulo.TabIndex = 0;
            this.BarraTitulo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.BarraTitulo_MouseDown);
            // 
            // MenuVertical
            // 
            this.MenuVertical.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.MenuVertical.Controls.Add(this.panel3);
            this.MenuVertical.Controls.Add(this.panel1);
            this.MenuVertical.Controls.Add(this.BTN_producto);
            this.MenuVertical.Controls.Add(this.pictureBox1);
            this.MenuVertical.Dock = System.Windows.Forms.DockStyle.Left;
            this.MenuVertical.Location = new System.Drawing.Point(0, 35);
            this.MenuVertical.Name = "MenuVertical";
            this.MenuVertical.Size = new System.Drawing.Size(220, 615);
            this.MenuVertical.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.panel2);
            this.panel3.Controls.Add(this.button1);
            this.panel3.Controls.Add(this.pictureBox2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(220, 615);
            this.panel3.TabIndex = 3;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel5.Controls.Add(this.panel10);
            this.panel5.Controls.Add(this.BTN_Cerrarsesion);
            this.panel5.Controls.Add(this.panel17);
            this.panel5.Controls.Add(this.Reporte);
            this.panel5.Controls.Add(this.Sub_menuRep);
            this.panel5.Controls.Add(this.panel11);
            this.panel5.Controls.Add(this.panel12);
            this.panel5.Controls.Add(this.panel13);
            this.panel5.Controls.Add(this.Tipo_Producto);
            this.panel5.Controls.Add(this.Cliente);
            this.panel5.Controls.Add(this.panel9);
            this.panel5.Controls.Add(this.venta);
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Controls.Add(this.panel4);
            this.panel5.Controls.Add(this.button2);
            this.panel5.Controls.Add(this.pictureBox3);
            this.panel5.Controls.Add(this.Compra);
            this.panel5.Controls.Add(this.devoluciones);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(220, 615);
            this.panel5.TabIndex = 4;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.DarkBlue;
            this.panel10.Location = new System.Drawing.Point(0, 581);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(5, 34);
            this.panel10.TabIndex = 5;
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.DarkBlue;
            this.panel17.Location = new System.Drawing.Point(1, 498);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(5, 45);
            this.panel17.TabIndex = 4;
            // 
            // Sub_menuRep
            // 
            this.Sub_menuRep.Controls.Add(this.panel16);
            this.Sub_menuRep.Controls.Add(this.Det_Devolucion);
            this.Sub_menuRep.Controls.Add(this.panel14);
            this.Sub_menuRep.Controls.Add(this.panel15);
            this.Sub_menuRep.Controls.Add(this.Det_ventas);
            this.Sub_menuRep.Controls.Add(this.Det_com);
            this.Sub_menuRep.Location = new System.Drawing.Point(58, 494);
            this.Sub_menuRep.Name = "Sub_menuRep";
            this.Sub_menuRep.Size = new System.Drawing.Size(162, 117);
            this.Sub_menuRep.TabIndex = 10;
            this.Sub_menuRep.Visible = false;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.DarkBlue;
            this.panel16.Location = new System.Drawing.Point(0, 84);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(5, 37);
            this.panel16.TabIndex = 6;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.DarkBlue;
            this.panel14.Location = new System.Drawing.Point(0, 3);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(5, 37);
            this.panel14.TabIndex = 4;
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.DarkBlue;
            this.panel15.Location = new System.Drawing.Point(0, 43);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(5, 37);
            this.panel15.TabIndex = 5;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.DarkBlue;
            this.panel11.Location = new System.Drawing.Point(0, 447);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(5, 45);
            this.panel11.TabIndex = 3;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.DarkBlue;
            this.panel12.Location = new System.Drawing.Point(0, 275);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(5, 48);
            this.panel12.TabIndex = 3;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.DarkBlue;
            this.panel13.Location = new System.Drawing.Point(0, 333);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(5, 48);
            this.panel13.TabIndex = 3;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.DarkBlue;
            this.panel9.Location = new System.Drawing.Point(0, 391);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(5, 45);
            this.panel9.TabIndex = 3;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.DarkBlue;
            this.panel6.Location = new System.Drawing.Point(0, 221);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(5, 48);
            this.panel6.TabIndex = 4;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.DarkBlue;
            this.panel4.Location = new System.Drawing.Point(0, 165);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(5, 48);
            this.panel4.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DarkBlue;
            this.panel2.Location = new System.Drawing.Point(0, 173);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(5, 32);
            this.panel2.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkBlue;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(3, 173);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(217, 32);
            this.button1.TabIndex = 1;
            this.button1.Text = "PRODUCTOS";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkBlue;
            this.panel1.Location = new System.Drawing.Point(0, 173);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(5, 32);
            this.panel1.TabIndex = 2;
            // 
            // BTN_producto
            // 
            this.BTN_producto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.BTN_producto.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.BTN_producto.FlatAppearance.BorderSize = 0;
            this.BTN_producto.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkBlue;
            this.BTN_producto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTN_producto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_producto.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.BTN_producto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BTN_producto.Location = new System.Drawing.Point(3, 173);
            this.BTN_producto.Name = "BTN_producto";
            this.BTN_producto.Size = new System.Drawing.Size(217, 32);
            this.BTN_producto.TabIndex = 1;
            this.BTN_producto.Text = "PRODUCTOS";
            this.BTN_producto.UseVisualStyleBackColor = false;
            // 
            // PanelContenedor
            // 
            this.PanelContenedor.BackColor = System.Drawing.Color.LightGray;
            this.PanelContenedor.Controls.Add(this.panel7);
            this.PanelContenedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelContenedor.Location = new System.Drawing.Point(220, 35);
            this.PanelContenedor.Name = "PanelContenedor";
            this.PanelContenedor.Size = new System.Drawing.Size(1080, 615);
            this.PanelContenedor.TabIndex = 2;
            this.PanelContenedor.Paint += new System.Windows.Forms.PaintEventHandler(this.PanelContenedor_Paint);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.LightGray;
            this.panel7.Controls.Add(this.panel8);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1080, 615);
            this.panel7.TabIndex = 5;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.LightGray;
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(1080, 615);
            this.panel8.TabIndex = 7;
            this.panel8.Paint += new System.Windows.Forms.PaintEventHandler(this.panel8_Paint);
            this.panel8.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel8_MouseDown);
            // 
            // BTN_Cerrarsesion
            // 
            this.BTN_Cerrarsesion.Image = global::SistemaGestionVentas.Properties.Resources.shutdown_105168;
            this.BTN_Cerrarsesion.Location = new System.Drawing.Point(7, 578);
            this.BTN_Cerrarsesion.Name = "BTN_Cerrarsesion";
            this.BTN_Cerrarsesion.Size = new System.Drawing.Size(34, 34);
            this.BTN_Cerrarsesion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BTN_Cerrarsesion.TabIndex = 12;
            this.BTN_Cerrarsesion.TabStop = false;
            this.BTN_Cerrarsesion.Click += new System.EventHandler(this.BTN_Cerrarsesion_Click);
            // 
            // Reporte
            // 
            this.Reporte.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Reporte.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Reporte.FlatAppearance.BorderSize = 0;
            this.Reporte.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkBlue;
            this.Reporte.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Reporte.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Reporte.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Reporte.Image = global::SistemaGestionVentas.Properties.Resources.pen_77896;
            this.Reporte.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Reporte.Location = new System.Drawing.Point(7, 498);
            this.Reporte.Name = "Reporte";
            this.Reporte.Size = new System.Drawing.Size(48, 45);
            this.Reporte.TabIndex = 11;
            this.Reporte.Text = "DET.";
            this.Reporte.UseVisualStyleBackColor = false;
            this.Reporte.Click += new System.EventHandler(this.Reporte_Click);
            // 
            // Det_Devolucion
            // 
            this.Det_Devolucion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Det_Devolucion.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Det_Devolucion.FlatAppearance.BorderSize = 0;
            this.Det_Devolucion.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkBlue;
            this.Det_Devolucion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Det_Devolucion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Det_Devolucion.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Det_Devolucion.Image = global::SistemaGestionVentas.Properties.Resources._353430_checkbox_edit_pen_pencil_107516;
            this.Det_Devolucion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Det_Devolucion.Location = new System.Drawing.Point(3, 80);
            this.Det_Devolucion.Name = "Det_Devolucion";
            this.Det_Devolucion.Size = new System.Drawing.Size(159, 37);
            this.Det_Devolucion.TabIndex = 13;
            this.Det_Devolucion.Text = "DET_DEVOLUCION";
            this.Det_Devolucion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Det_Devolucion.UseVisualStyleBackColor = false;
            this.Det_Devolucion.Click += new System.EventHandler(this.Det_Devolucion_Click);
            // 
            // Det_ventas
            // 
            this.Det_ventas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Det_ventas.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Det_ventas.FlatAppearance.BorderSize = 0;
            this.Det_ventas.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkBlue;
            this.Det_ventas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Det_ventas.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Det_ventas.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Det_ventas.Image = global::SistemaGestionVentas.Properties.Resources._1492532798_9_basket_83229;
            this.Det_ventas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Det_ventas.Location = new System.Drawing.Point(3, 43);
            this.Det_ventas.Name = "Det_ventas";
            this.Det_ventas.Size = new System.Drawing.Size(159, 37);
            this.Det_ventas.TabIndex = 12;
            this.Det_ventas.Text = "DETALLE VENTAS";
            this.Det_ventas.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Det_ventas.UseVisualStyleBackColor = false;
            this.Det_ventas.Click += new System.EventHandler(this.Det_ventas_Click);
            // 
            // Det_com
            // 
            this.Det_com.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Det_com.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Det_com.FlatAppearance.BorderSize = 0;
            this.Det_com.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkBlue;
            this.Det_com.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Det_com.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Det_com.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Det_com.Image = global::SistemaGestionVentas.Properties.Resources.pencil_writing_on_a_paper_sheet_icon_icons_com_70422;
            this.Det_com.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Det_com.Location = new System.Drawing.Point(3, 3);
            this.Det_com.Name = "Det_com";
            this.Det_com.Size = new System.Drawing.Size(159, 37);
            this.Det_com.TabIndex = 11;
            this.Det_com.Text = "DETALLE COMPRAS";
            this.Det_com.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Det_com.UseVisualStyleBackColor = false;
            this.Det_com.Click += new System.EventHandler(this.Det_com_Click);
            // 
            // Tipo_Producto
            // 
            this.Tipo_Producto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Tipo_Producto.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Tipo_Producto.FlatAppearance.BorderSize = 0;
            this.Tipo_Producto.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkBlue;
            this.Tipo_Producto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Tipo_Producto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Tipo_Producto.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Tipo_Producto.Image = global::SistemaGestionVentas.Properties.Resources.business_inventory_maintenance_product_box_boxes_2326;
            this.Tipo_Producto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Tipo_Producto.Location = new System.Drawing.Point(3, 447);
            this.Tipo_Producto.Name = "Tipo_Producto";
            this.Tipo_Producto.Size = new System.Drawing.Size(217, 45);
            this.Tipo_Producto.TabIndex = 9;
            this.Tipo_Producto.Text = "TIPO PRODUCTO";
            this.Tipo_Producto.UseVisualStyleBackColor = false;
            this.Tipo_Producto.Click += new System.EventHandler(this.Tipo_Producto_Click);
            // 
            // Cliente
            // 
            this.Cliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Cliente.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Cliente.FlatAppearance.BorderSize = 0;
            this.Cliente.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkBlue;
            this.Cliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cliente.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Cliente.Image = global::SistemaGestionVentas.Properties.Resources.person_user_customer_man_male_man_boy_people_1687;
            this.Cliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Cliente.Location = new System.Drawing.Point(3, 333);
            this.Cliente.Name = "Cliente";
            this.Cliente.Size = new System.Drawing.Size(217, 48);
            this.Cliente.TabIndex = 7;
            this.Cliente.Text = "CLIENTE";
            this.Cliente.UseVisualStyleBackColor = false;
            this.Cliente.Click += new System.EventHandler(this.Cliente_Click);
            // 
            // venta
            // 
            this.venta.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.venta.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.venta.FlatAppearance.BorderSize = 0;
            this.venta.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkBlue;
            this.venta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.venta.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.venta.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.venta.Image = global::SistemaGestionVentas.Properties.Resources.Sales_report_25411;
            this.venta.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.venta.Location = new System.Drawing.Point(3, 275);
            this.venta.Name = "venta";
            this.venta.Size = new System.Drawing.Size(217, 48);
            this.venta.TabIndex = 6;
            this.venta.Text = "VENTA";
            this.venta.UseVisualStyleBackColor = false;
            this.venta.Click += new System.EventHandler(this.venta_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkBlue;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button2.Image = global::SistemaGestionVentas.Properties.Resources.shipping_products_22121;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(3, 165);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(217, 48);
            this.button2.TabIndex = 1;
            this.button2.Text = "PRODUCTOS";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(0, 3);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(220, 146);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 0;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox3_MouseDown);
            // 
            // Compra
            // 
            this.Compra.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Compra.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Compra.FlatAppearance.BorderSize = 0;
            this.Compra.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkBlue;
            this.Compra.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Compra.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Compra.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Compra.Image = global::SistemaGestionVentas.Properties.Resources.shopping_cart_symbol_for_e_commerce_icon_icons_com_56124;
            this.Compra.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Compra.Location = new System.Drawing.Point(3, 221);
            this.Compra.Name = "Compra";
            this.Compra.Size = new System.Drawing.Size(217, 48);
            this.Compra.TabIndex = 3;
            this.Compra.Text = "COMPRA";
            this.Compra.UseVisualStyleBackColor = false;
            this.Compra.Click += new System.EventHandler(this.Compra_Click);
            // 
            // devoluciones
            // 
            this.devoluciones.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.devoluciones.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.devoluciones.FlatAppearance.BorderSize = 0;
            this.devoluciones.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkBlue;
            this.devoluciones.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.devoluciones.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.devoluciones.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.devoluciones.Image = global::SistemaGestionVentas.Properties.Resources._4544831_business_comerce_delivery_return_shop_121431;
            this.devoluciones.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.devoluciones.Location = new System.Drawing.Point(3, 391);
            this.devoluciones.Name = "devoluciones";
            this.devoluciones.Size = new System.Drawing.Size(217, 48);
            this.devoluciones.TabIndex = 8;
            this.devoluciones.Text = "DEVOLUCIONES";
            this.devoluciones.UseVisualStyleBackColor = false;
            this.devoluciones.Click += new System.EventHandler(this.devoluciones_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(0, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(220, 146);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(220, 146);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // BTNcerrar_inicio
            // 
            this.BTNcerrar_inicio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BTNcerrar_inicio.Image = global::SistemaGestionVentas.Properties.Resources.window_close_icon_153323;
            this.BTNcerrar_inicio.Location = new System.Drawing.Point(1266, 2);
            this.BTNcerrar_inicio.Name = "BTNcerrar_inicio";
            this.BTNcerrar_inicio.Size = new System.Drawing.Size(31, 33);
            this.BTNcerrar_inicio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.BTNcerrar_inicio.TabIndex = 0;
            this.BTNcerrar_inicio.TabStop = false;
            this.BTNcerrar_inicio.Click += new System.EventHandler(this.BTNcerrar_inicio_Click);
            // 
            // BTNminimizar_inicio
            // 
            this.BTNminimizar_inicio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BTNminimizar_inicio.Image = global::SistemaGestionVentas.Properties.Resources.browser_minimize_icon_177736;
            this.BTNminimizar_inicio.Location = new System.Drawing.Point(1192, 5);
            this.BTNminimizar_inicio.Name = "BTNminimizar_inicio";
            this.BTNminimizar_inicio.Size = new System.Drawing.Size(31, 27);
            this.BTNminimizar_inicio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.BTNminimizar_inicio.TabIndex = 1;
            this.BTNminimizar_inicio.TabStop = false;
            this.BTNminimizar_inicio.Click += new System.EventHandler(this.BTNminimizar_inicio_Click);
            // 
            // BTNMaximizar_inicio
            // 
            this.BTNMaximizar_inicio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BTNMaximizar_inicio.Image = global::SistemaGestionVentas.Properties.Resources.window_restore_icon_137011;
            this.BTNMaximizar_inicio.Location = new System.Drawing.Point(1227, 2);
            this.BTNMaximizar_inicio.Name = "BTNMaximizar_inicio";
            this.BTNMaximizar_inicio.Size = new System.Drawing.Size(41, 35);
            this.BTNMaximizar_inicio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.BTNMaximizar_inicio.TabIndex = 2;
            this.BTNMaximizar_inicio.TabStop = false;
            this.BTNMaximizar_inicio.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // BTNRestaurar
            // 
            this.BTNRestaurar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BTNRestaurar.Image = global::SistemaGestionVentas.Properties.Resources.window_maximize_icon_137012;
            this.BTNRestaurar.Location = new System.Drawing.Point(1227, 3);
            this.BTNRestaurar.Name = "BTNRestaurar";
            this.BTNRestaurar.Size = new System.Drawing.Size(39, 32);
            this.BTNRestaurar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.BTNRestaurar.TabIndex = 3;
            this.BTNRestaurar.TabStop = false;
            this.BTNRestaurar.Visible = false;
            this.BTNRestaurar.Click += new System.EventHandler(this.BTNRestaurar_Click);
            // 
            // InicioForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1300, 650);
            this.Controls.Add(this.PanelContenedor);
            this.Controls.Add(this.MenuVertical);
            this.Controls.Add(this.BarraTitulo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "InicioForm";
            this.Text = "InicioForm";
            this.BarraTitulo.ResumeLayout(false);
            this.MenuVertical.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.Sub_menuRep.ResumeLayout(false);
            this.PanelContenedor.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BTN_Cerrarsesion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTNcerrar_inicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTNminimizar_inicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTNMaximizar_inicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTNRestaurar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel BarraTitulo;
        private System.Windows.Forms.Panel MenuVertical;
        private System.Windows.Forms.Panel PanelContenedor;
        private System.Windows.Forms.PictureBox BTNcerrar_inicio;
        private System.Windows.Forms.PictureBox BTNminimizar_inicio;
        private System.Windows.Forms.PictureBox BTNMaximizar_inicio;
        private System.Windows.Forms.PictureBox BTNRestaurar;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button BTN_producto;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Button Tipo_Producto;
        private System.Windows.Forms.Button devoluciones;
        private System.Windows.Forms.Button Cliente;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Button venta;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button Compra;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel Sub_menuRep;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Button Det_Devolucion;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Button Det_ventas;
        private System.Windows.Forms.Button Det_com;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Button Reporte;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.PictureBox BTN_Cerrarsesion;
    }
}