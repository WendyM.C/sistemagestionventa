﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;


namespace DataAccess
{
    public class UserDao : ConeccionSQL
    {
        public bool Login(String User, String Pass)
        {
            using (var connection = GetConnection())
            {
                connection.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = " select *from Users where LoginName=@User  and password=@pass ";
                    command.Parameters.AddWithValue("@User", User);
                    command.Parameters.AddWithValue("@pass", Pass);
                    command.CommandType = CommandType.Text;
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        return true;
                    }
                    else
                        return false;
                }
            }
        }

    }
}
